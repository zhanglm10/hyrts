/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.common;

import java.io.File;
import java.io.PrintStream;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Plugin;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.surefire.SurefirePlugin;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.twdata.maven.mojoexecutor.MojoExecutor;

import set.hyrts.coverage.maven.DynamicAgentLoader;
import set.hyrts.diff.VersionDiff;
import set.hyrts.utils.Properties;
import set.hyrts.utils.Properties.RTSVariant;

//@Mojo(name = "test", defaultPhase = LifecyclePhase.TEST, requiresDependencyResolution = ResolutionScope.TEST)
public class AbstractCoverageMojo extends SurefirePlugin
{
	/**
	 * Location of the binary directory
	 */
	@Parameter(defaultValue = "${project.build.directory}", property = "outputDir", required = true)
	public File outputDirectory;

	/**
	 * The current maven project under test
	 */
	@Parameter(property = "project")
	protected MavenProject mavenProject;

	/**
	 * The dir of the maven project under test
	 */
	@Parameter(defaultValue = "${basedir}")
	protected String baseDir;

	/**
	 * Version of the current hyrts
	 */
	@Parameter(defaultValue = "1.0-SNAPSHOT")
	protected String version;

	/**
	 * The level of coverage to collect
	 */
	@Parameter(defaultValue = "meth-cov")
	protected String coverageLevel;

	/**
	 * The RTS Variant to perform
	 */
	@Parameter(property = "RTS", defaultValue = "NONE")
	protected RTSVariant RTS;

	/**
	 * Test granularity
	 */
	@Parameter(property = "testMethLevel", defaultValue = "false")
	protected boolean testMethLevel;

	/**
	 * Whether to collect coverage or not
	 */
	@Parameter(property = "execOnly", defaultValue = "false")
	protected boolean execOnly;

	/**
	 * The hybrid level for hyrts
	 */
	@Parameter(property = "hybridLevel")
	protected String hybridLevel;

	/**
	 * The location of the old version, default: the current dir
	 */
	@Parameter(property = "oldVersion", defaultValue = "${basedir}")
	protected String oldVersionLocation;

	/**
	 * Whether to trace 3rd party libs
	 */
	@Parameter(property = "traceLib", defaultValue = "false")
	protected boolean traceLib;

	/**
	 * Whether to trace runtime info
	 */
	@Parameter(property = "traceRTInfo", defaultValue = "true")
	protected boolean traceRTInfo;

	/**
	 * The location of the agent jar
	 */
	@Parameter(property = "agentJar", defaultValue = "")
	protected String agentJar;

	/**
	 * Whether to skip integration tests
	 */
	@Parameter(property = "skipIT", defaultValue = "false")
	protected boolean skipIT;

	/**
	 * Whether to print debug info
	 */
	@Parameter(property = "debug", defaultValue = "false")
	protected boolean debug;

	@Component
	protected MavenSession mavenSession;
	@Component
	protected BuildPluginManager pluginManager;

	protected Plugin surefire;
	protected Plugin failsafe;

	public void execute() throws MojoExecutionException {
		//System.out.println("Start HyRTS");
		//long start=System.currentTimeMillis();
		agentJar = fasterGetPathToHyRTSJar();
		String arguments = getConfigs();

		// perform RTS to exclude tests
		if (RTS != RTSVariant.NONE)
			RTSUtil.getRTSRes(this);

		// set the base dir for coverage collection
		// System.setProperty(Properties.NEW_DIR_KEY, this.baseDir.toString());
		DynamicAgentLoader.loadDynamicAgent(agentJar, arguments);
		
		//long mid=System.currentTimeMillis();
		//System.out.println("Before test "+(mid-start)+" ms");
		runSurefire();
		if (!skipIT)
			runIntegrationTests();
		//long end=System.currentTimeMillis();
		//System.out.println("Finish HyRTS in "+(end-start)+" ms");
	}

	private void runSurefire() throws MojoExecutionException {
		this.surefire = this
				.lookupPlugin("org.apache.maven.plugins:maven-surefire-plugin");
		if (this.surefire == null) {
			getLog().error("Make sure surefire is in your pom.xml!");
		}
		Xpp3Dom domNode = (Xpp3Dom) this.surefire.getConfiguration();
		if (domNode == null)
			domNode = new Xpp3Dom("configuration");
		// domNode = transformPom(domNode);
		MojoExecutor.executeMojo(this.surefire, "test", domNode,
				MojoExecutor.executionEnvironment(this.mavenProject,
						this.mavenSession, this.pluginManager));
	}

	private void runIntegrationTests() throws MojoExecutionException {
		this.failsafe = this
				.lookupPlugin("org.apache.maven.plugins:maven-failsafe-plugin");

		if (this.failsafe != null) {
			Xpp3Dom failsafeDomNode = (Xpp3Dom) this.failsafe
					.getConfiguration();
			if (failsafeDomNode == null)
				failsafeDomNode = new Xpp3Dom("configuration");
			MojoExecutor.executeMojo(this.failsafe, "integration-test",
					failsafeDomNode,
					MojoExecutor.executionEnvironment(this.mavenProject,
							this.mavenSession, this.pluginManager));
		}
	}

	private Xpp3Dom transformPom(Xpp3Dom domNode) {
		getLog().info("outputDir: " + outputDirectory.getAbsolutePath());
		getLog().info("current project: " + mavenProject.toString());
		// getLog().info("properties: " +
		// mavenProject.getProperties().toString());
		getLog().info("baseDir: " + baseDir);

		// execure maven surefire tests
		// mavenProject.getProperties().setProperty("argLine", "");

		if (domNode == null) {
			domNode = new Xpp3Dom("configuration");
		}
		getLog().info("Original surefire config: \n" + domNode.toString());

		if (domNode.getChild("argLine") != null) {
			String originalArgLine = domNode.getChild("argLine").getValue();
		} else {
			domNode.addChild(new Xpp3Dom("argLine"));
		}

		String hyrtsArgLine = getHyRTSArgLine();
		domNode.getChild("argLine").setValue(hyrtsArgLine);

		getLog().info(this.mavenProject + "\n" + this.mavenSession + "\n"
				+ this.pluginManager);
		getLog().info("Modified surefire config: \n" + domNode.toString());
		return domNode;
	}

	private String getHyRTSArgLine() {
		String jarLocation = getPathToHyRTSJar();
		String arguments = getConfigs();
		return "-javaagent:" + jarLocation + "=" + arguments;
	}

	private String getConfigs() {
		String testLevel = testMethLevel ? Properties.TM_LEVEL
				: Properties.TC_LEVEL;

		// does not support RTS for test method level or for branch/stmt
		// coverage
		if (testLevel.equals(Properties.TM_LEVEL)
				|| Properties.BRANCH_COV.equals(coverageLevel)
				|| Properties.STMT_COV.equals(coverageLevel)) {
			RTS = RTSVariant.NONE;
		}

		// distinguish coverage file for different RTS apps
		if (RTS != RTSVariant.NONE) {
			coverageLevel = RTS + "-" + coverageLevel;
		}

		// config for hybrid RTS
		if (hybridLevel != null)
			readHybridConfig(hybridLevel);

		String arguments = Properties.DEBUG_MODE_KEY + this.debug + ","
				+ Properties.RTS_KEY + this.RTS + "," 
				+ (this.coverageLevel!=null? Properties.COV_TYPE_KEY + this.coverageLevel + "," :"")
				+ Properties.TEST_LEVEL_KEY
				+ testLevel + "," + Properties.TRACE_LIB_KEY + this.traceLib
				+ "," + Properties.TRACE_RTINFO_KEY + this.traceRTInfo + ","
				+ Properties.OLD_DIR_KEY + this.oldVersionLocation + ","
				+ Properties.NEW_DIR_KEY + this.baseDir.toString() + ","
				+ Properties.NEW_CLASSPATH_KEY + this.outputDirectory + ","
				+ Properties.EXECUTION_ONLY_KEY + this.execOnly;

		if (debug) {
			PrintStream printer = System.out;
			printer.println(Properties.HYRTS_TAG
					+ "===============Plugin Config===============");
			printer.println(Properties.HYRTS_TAG + "RTS type: " + RTS);
			printer.println(
					Properties.HYRTS_TAG + "Coverage level: " + coverageLevel);
			printer.println(
					Properties.HYRTS_TAG + "Test level: " + (testMethLevel
							? Properties.TM_LEVEL : Properties.TC_LEVEL));
			printer.println(
					Properties.HYRTS_TAG + "Trace library: " + traceLib);
			printer.println(Properties.HYRTS_TAG + "With runTime info?: "
					+ traceRTInfo);
			printer.println(Properties.HYRTS_TAG + "Old version location: "
					+ oldVersionLocation);
			printer.println(
					Properties.HYRTS_TAG + "New version location: " + baseDir);
			printer.println(Properties.HYRTS_TAG
					+ "New version classpath location: " + outputDirectory);

			printer.println(Properties.HYRTS_TAG + "Hybrid config: "
					+ VersionDiff.transDSI + "|" + VersionDiff.transASI + "|"
					+ VersionDiff.transCSI + "|" + VersionDiff.transDI + "|"
					+ VersionDiff.transAI + "|" + VersionDiff.transCI + "|"
					+ VersionDiff.transDSM + "|" + VersionDiff.transASM + "|"
					+ VersionDiff.transCSM + "|" + VersionDiff.transDIM + "|"
					+ VersionDiff.transAIM + "|" + VersionDiff.transCIM + "|");

			printer.println(Properties.HYRTS_TAG + "Execution only config: "
					+ execOnly);
		}

		return arguments;
	}

	private Plugin lookupPlugin(String paramString) {
		List<Plugin> localList = this.mavenProject.getBuildPlugins();
		Iterator<Plugin> localIterator = localList.iterator();
		while (localIterator.hasNext()) {
			Plugin localPlugin = localIterator.next();
			if (paramString.equalsIgnoreCase(localPlugin.getKey())) {
				return localPlugin;
			}
		}
		return null;
	}

	private String getPathToHyRTSJar() {
		getHyRTSVersion();
		String localRepo = this.mavenSession.getSettings().getLocalRepository();
		String result = Paths
				.get(mavenSession.getLocalRepository().getBasedir(), "set",
						"hyrts", "hyrts-core", version,
						"hyrts-core-" + version + ".jar")
				.toString();
		return result;
	}
	
	private String fasterGetPathToHyRTSJar(){
		File jarFile = new File(Properties.class.getProtectionDomain()
				.getCodeSource().getLocation().getPath());
		String agentJar = jarFile.toString();
		return agentJar;
	}

	private void getHyRTSVersion() {
		Artifact artifact = (Artifact) this.mavenProject.getPluginArtifactMap()
				.get("set.hyrts:hyrts-maven-plugin");
		this.version = artifact.getVersion();
	}

	/**
	 * Read hybrid config information
	 * 
	 * @param hybridInfo hybrid input config
	 */
	public void readHybridConfig(String hybridInfo) {
		for (int i = 0; i < hybridInfo.length(); i++) {
			char tag = hybridInfo.charAt(i);
			if (tag > 'z' || tag < 'a')
				continue;
			switch (tag) {
			case 'a':
				VersionDiff.transDSI = true;
				break;
			case 'b':
				VersionDiff.transASI = true;
				break;
			case 'c':
				VersionDiff.transCSI = true;
				break;
			case 'd':
				VersionDiff.transDI = true;
				break;
			case 'e':
				VersionDiff.transAI = true;
				break;
			case 'f':
				VersionDiff.transCI = true;
				break;
			case 'g':
				VersionDiff.transDSM = true;
				break;
			case 'h':
				VersionDiff.transASM = true;
				break;
			case 'i':
				VersionDiff.transCSM = true;
				break;
			case 'j':
				VersionDiff.transDIM = true;
				break;
			case 'k':
				VersionDiff.transAIM = true;
				break;
			case 'l':
				VersionDiff.transCIM = true;
				break;
			}
		}
		// if both AIM and DIM have been transformed, no need for tracing
		// runtime info
		if (VersionDiff.transAIM && VersionDiff.transDIM) {
			traceRTInfo = false;
		}
	}

}
