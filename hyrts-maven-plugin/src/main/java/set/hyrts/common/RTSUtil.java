package set.hyrts.common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import set.hyrts.coverage.agent.JUnitAgent;
import set.hyrts.coverage.maven.SurefireRewriter;
import set.hyrts.rts.HybridRTS;
import set.hyrts.rts.HybridRTSWithBlock;
import set.hyrts.rts.MethRTS;
import set.hyrts.utils.Properties;
import set.hyrts.utils.Properties.RTSVariant;

public class RTSUtil
{
	/**
	 * Get the excluded list from RTS techs
	 * @param mojo mojo input
	 */
	public static void getRTSRes(AbstractCoverageMojo mojo) {
		Properties.OLD_DIR = mojo.oldVersionLocation;
		Properties.NEW_DIR = mojo.baseDir;
		Properties.NEW_CLASSPATH = mojo.outputDirectory.getAbsolutePath();
		Properties.TRACER_COV_TYPE = mojo.coverageLevel;
		Properties.RTS = mojo.RTS;
		if (Properties.RTS != RTSVariant.NONE) {
			Properties.FILE_CHECKSUM = Properties.RTS + "-checksum";
		}

		Set<String> excluded = new HashSet<String>();
		try {
			if (mojo.RTS == RTSVariant.FRTS || mojo.RTS == RTSVariant.HyRTS
					|| mojo.RTS == RTSVariant.HyRTSf) {
				excluded = HybridRTS.main();
			} else if (mojo.RTS == RTSVariant.HyRTSb) {
				excluded = HybridRTSWithBlock.main();
			} else if (mojo.RTS == RTSVariant.MRTS) {
				excluded = MethRTS.main();
			}
			serializeJUnit3Excludes(excluded,
					JUnitAgent.getJUnit3ExcludePath());
		} catch (Exception e) {
			e.printStackTrace();
		}
		excluded = toJUnit4SurefireExcludeFormat(excluded);
		System.setProperty(SurefireRewriter.HYRTS_EXCLUDED,
				Arrays.toString(excluded.toArray(new String[0])));
	}

	public static void serializeJUnit3Excludes(Set<String> excluded,
			String path) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(path));
		for (String item : excluded) {
			writer.write(item + "\n");
		}
		writer.flush();
		writer.close();
	}

	public static Set<String> deSerializeJUnit3Excludes(String path)
			throws IOException {
		Set<String> excluded = new HashSet<String>();
		File f = new File(path);
		if (!f.exists())
			return excluded;
		BufferedReader reader = new BufferedReader(new FileReader(path));
		String line = reader.readLine();
		while (line != null) {
			excluded.add(line);
			line = reader.readLine();
		}
		reader.close();
		return excluded;
	}

	public static Set<String> toJUnit4SurefireExcludeFormat(
			Set<String> excluded) {
		Set<String> transformed = new HashSet<String>();
		for (String item : excluded) {
			transformed.add(item.replace(".", File.separator) + ".*");
			// transformed.add("*/"+item.replace(".", File.separator) + ".*");
		}
		return transformed;
	}

}
