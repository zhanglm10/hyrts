/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.utils;

import java.io.PrintStream;

import set.hyrts.coverage.core.CoverageData;
import set.hyrts.diff.VersionDiff;
import set.hyrts.rts.HybridRTS;
import set.hyrts.rts.HybridRTSWithBlock;
import set.hyrts.rts.MethRTS;
import set.hyrts.utils.Properties.RTSVariant;

public class Properties
{
	public static final String CLASS = "set/hyrts/utils/Properties";
	public static final String TRACER_ROOT_DIR = "hyrts-files";
	public static final String GZ_EXT = ".gz";
	public static final String HYRTS_TAG = "[HyRTS] ";
	public static final String EXCLUDE = ".exclude";

	public static String TRACER_CLASS = "set/hyrts/coverage/io/Tracer";
	public static String FILE_CHECKSUM = null;

//	public static String AGENT_JAR_KEY = "agentJar=";
//	public static String AGENT_JAR = "";
	public static String AGENT_ARG = "";

	public static String COV_TYPE_KEY = "covLevel=";
	public static final String STMT_COV = "stmt-cov";
	public static final String BLK_COV = "block-cov";
	public static final String BRANCH_COV = "branch-cov";
	public static final String CLASS_COV = "class-cov";
	public static final String METH_COV = "meth-cov";
	// can also be "class-cov","meth-cov", "branch-cov", "block-cov",
	// "stmt-cov", or null
	public static String TRACER_COV_TYPE = null;

	public static String OLD_DIR_KEY = "oldVersion=";
	// by default treating the current dir as old dir as well
	public static String OLD_DIR = System.getProperty("user.dir");

	public static String NEW_DIR_KEY = "newVersion=";
	// by default treating the current dir as old dir as well
	public static String NEW_DIR = System.getProperty("user.dir");

	public static String NEW_CLASSPATH_KEY = "bytecodeLocation=";
	// by default search "target" dir for bytecode files
	public static String NEW_CLASSPATH = "target";

	public static String LEVEL_KEY = "hybridConfig=";

	public static String EXECUTION_ONLY_KEY = "execOnly=";
	// by default collect new coverage for each test
	public static boolean EXECUTION_ONLY = false;

	// by default do not trace lib coverage, but will when the current project
	// prefix is not assigned
	public static String TRACE_LIB_KEY = "traceLib=";
	public static boolean TRACE_LIB = false;

	// by default trace runtime info for hybrid RTS
	public static String TRACE_RTINFO_KEY = "traceRTInfo=";
	public static boolean TRACE_RTINFO = true;

	// by default do not print debug info
	public static String DEBUG_MODE_KEY = "debug=";
	public static boolean DEBUG_MODE = false;

	public static String RTS_KEY = "RTS=";

	public enum RTSVariant {
		NONE, MRTS, HyRTS, FRTS, HyRTSf, HyRTSb
	};

	// by default always perform RTS
	public static RTSVariant RTS = RTSVariant.NONE;

	public static String TEST_LEVEL_KEY = "testLevel=";
	public final static String TM_LEVEL = "test-meth";
	public final static String TC_LEVEL = "test-class";
	// by default collect test-class level information
	public static String TEST_LEVEL = TC_LEVEL;

	public static void processAgentMainArguments(String arguments) {
		if (arguments == null)
			return;
		AGENT_ARG = arguments;
//		String[] items = arguments.split(",");
//		for (String item : items) {
//			if (item.startsWith(RTS_KEY)) {
//				RTS = RTSVariant.valueOf(item.replace(RTS_KEY, ""));
//			} else if (item.startsWith(COV_TYPE_KEY)) {
//				TRACER_COV_TYPE = item.replace(COV_TYPE_KEY, "");
//			} else if (item.startsWith(DEBUG_MODE_KEY)) {
//				DEBUG_MODE = Boolean
//						.parseBoolean(item.replace(DEBUG_MODE_KEY, ""));
//			} else if (item.startsWith(TRACE_LIB_KEY)) {
//				TRACE_LIB = Boolean
//						.parseBoolean(item.replace(TRACE_LIB_KEY, ""));
//			} else if (item.startsWith(TEST_LEVEL_KEY)) {
//				TEST_LEVEL = item.replace(TEST_LEVEL_KEY, "");
//			} else if (item.startsWith(NEW_DIR_KEY)) {
//				NEW_DIR = item.replace(NEW_DIR_KEY, "");
//			} else if (item.startsWith(EXECUTION_ONLY_KEY)) {
//				EXECUTION_ONLY = Boolean
//						.parseBoolean(item.replace(EXECUTION_ONLY_KEY, ""));
//			} else if (item.startsWith(AGENT_JAR_KEY)) {
//				AGENT_JAR = item.replace(AGENT_JAR_KEY, "");
//			}

			// else if (item.startsWith(LEVEL_KEY)) {
			// String hybridInfo = item.replace(LEVEL_KEY, "");
			// readHybridConfig(hybridInfo);
			// } else if (item.startsWith(OLD_DIR_KEY)) {
			// OLD_DIR = item.replace(OLD_DIR_KEY, "");
			// } else if (item.startsWith(NEW_CLASSPATH_KEY)) {
			// NEW_CLASSPATH = item.replace(NEW_CLASSPATH_KEY, "");
			// }
		//}

	}

	public static void processPreMainArguments(String arguments) {
		if (arguments == null)
			return;
		AGENT_ARG = arguments;
		String[] items = arguments.split(",");
		for (String item : items) {
			if (item.startsWith(RTS_KEY)) {
				RTS = RTSVariant.valueOf(item.replace(RTS_KEY, ""));
			} else if (item.startsWith(COV_TYPE_KEY)) {
				TRACER_COV_TYPE = item.replace(COV_TYPE_KEY, "");
			} else if (item.startsWith(TEST_LEVEL_KEY)) {
				TEST_LEVEL = item.replace(TEST_LEVEL_KEY, "");
			} else if (item.startsWith(TRACE_LIB_KEY)) {
				TRACE_LIB = Boolean
						.parseBoolean(item.replace(TRACE_LIB_KEY, ""));
			} else if (item.startsWith(TRACE_RTINFO_KEY)) {
				TRACE_RTINFO = Boolean
						.parseBoolean(item.replace(TRACE_RTINFO_KEY, ""));
			} /*else if (item.startsWith(NEW_DIR_KEY)) {
				NEW_DIR = item.replace(NEW_DIR_KEY, "");
			}*/ else if (item.startsWith(EXECUTION_ONLY_KEY)) {
				EXECUTION_ONLY = Boolean
						.parseBoolean(item.replace(EXECUTION_ONLY_KEY, ""));
			} else if (item.startsWith(DEBUG_MODE_KEY)) {
				DEBUG_MODE = Boolean.parseBoolean(item.replace(DEBUG_MODE_KEY, ""));
			}
		}
		
		if (DEBUG_MODE) {
			PrintStream printer = System.out;
			printer.println(Properties.HYRTS_TAG+"===============PreMain Config===============");
			printer.println(
					Properties.HYRTS_TAG + "Coverage level: " + TRACER_COV_TYPE);
			printer.println(
					Properties.HYRTS_TAG + "Test level: " + TEST_LEVEL);
			printer.println(
					Properties.HYRTS_TAG + "Trace library: " + TRACE_LIB);
			printer.println(Properties.HYRTS_TAG + "With RunTime Info?: "
					+ TRACE_RTINFO);
			printer.println(
					Properties.HYRTS_TAG + "New version location: " + NEW_DIR);
			printer.println(Properties.HYRTS_TAG + "Execution only config: "
					+ EXECUTION_ONLY);
		}
	}

}
