package set.hyrts.logger;

import set.hyrts.utils.Properties;

public class FTracerLogger
{
	static Levels level = Levels.DEBUG;
	static String DebugPrefix = "[HyRTS-DEBUG] ";
	static String InfoPrefix = "[HyRTS-INFO] ";
	static String WarnPrefix = "[HyRTS-WARN] ";

	public static void debug(String s) {
		if (level == Levels.DEBUG) {
			System.out.println(DebugPrefix + s);
		}

	}

	public static void info(String s) {
		if (level == Levels.DEBUG || level == Levels.INFO) {
			System.out.println(InfoPrefix + s);
		}
	}

	public static void warn(String s) {
		if (level == Levels.DEBUG || level == Levels.INFO
				|| level == Levels.WARN) {
			System.out.println(WarnPrefix + s);
		}
	}

}

enum Levels {
	DEBUG, INFO, WARN
};