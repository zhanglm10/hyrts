/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.coverage.junit;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import set.hyrts.coverage.core.CoverageData;
import set.hyrts.logger.FTracerLogger;
import set.hyrts.utils.Properties;

class JUnit3MethodVisitor extends MethodVisitor implements Opcodes
{

	public JUnit3MethodVisitor(final MethodVisitor mv) {
		super(ASM5, mv);
	}

	// @Override
	// public void visitCode() {
	// // if the test is excluded, then directly return without execution
	// mv.visitVarInsn(ALOAD, 0);
	// mv.visitFieldInsn(GETFIELD,
	// JUnitTestClassLevelTransformer.JUNIT38_RUNNER_CLASS, "fName",
	// "Ljava/lang/String;");
	// mv.visitMethodInsn(INVOKESTATIC, FTracerJUnitRunner.className,
	// FTracerJUnitRunner.isExcluded, "(Ljava/lang/String;)Z", false);
	// Label label = new Label();
	// mv.visitJumpInsn(IFEQ, label);
	// mv.visitInsn(RETURN);
	// mv.visitLabel(label);
	// // mv.visitInsn();
	// mv.visitCode();
	// }

	// transform the run invocation to dump coverage info
	// @Override
	// public void visitInsn(int opcode) {
	// if (opcode == RETURN) {
	// mv.visitVarInsn(ALOAD, 0);
	// mv.visitFieldInsn(GETFIELD,
	// JUnitTestClassLevelTransformer.JUNIT38_RUNNER_CLASS,
	// "fName", "Ljava/lang/String;");
	// mv.visitMethodInsn(INVOKESTATIC, FTracerJUnitRunner.className,
	// FTracerJUnitRunner.transJUnit3Runner,
	// "(Ljava/lang/String;)V", false);
	// }
	// mv.visitInsn(opcode);
	// }

	// transform addTestsFromTestCase to dump coverage
	// @Override
	// public void visitMethodInsn(int opcode, String owner, String name,
	// String desc, boolean itf) {
	// if (true// name.equals("addTestsFromTestCase")
	// /*
	// * && desc .equals("(Ljava/lang/Class;)V")
	// */) {
	// FTracerLogger.debug(
	// "addTestsFromTestCase captured " + name + "-" + desc);
	// mv.visitMethodInsn(INVOKESTATIC, FTracerJUnitRunner.className,
	// FTracerJUnitRunner.transJUnit3Runner,
	// "(Ljunit/framework/TestSuite;Ljava/lang/Class;)V", itf);
	// } else
	// mv.visitMethodInsn(opcode, owner, name, desc, itf);
	// }

	@Override
	public void visitFieldInsn(int opcode, String owner, String name,
			String desc) {
		this.mv.visitFieldInsn(opcode, owner, name, desc);
		if ((opcode == Opcodes.PUTFIELD)
				&& (owner.equals("junit/framework/TestSuite"))
				&& (name.equals("fTests"))
				&& (desc.equals("Ljava/util/Vector;"))) {
			Label localLabel = new Label();
			this.mv.visitVarInsn(ALOAD, 0);
			this.mv.visitVarInsn(ALOAD, 1);
			mv.visitMethodInsn(INVOKESTATIC, FTracerJUnitUtils.className,
					FTracerJUnitUtils.transJUnit3SuiteInit,
					"(Ljunit/framework/TestSuite;Ljava/lang/Class;)Z", false);
			this.mv.visitJumpInsn(IFNE, localLabel);
			this.mv.visitInsn(RETURN);
			this.mv.visitLabel(localLabel);
		}
	}

}