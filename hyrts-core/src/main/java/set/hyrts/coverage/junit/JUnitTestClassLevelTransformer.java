/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.coverage.junit;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

import org.apache.log4j.Logger;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import set.hyrts.coverage.core.CoverageData;
import set.hyrts.logger.FTracerLogger;
import set.hyrts.utils.Properties;

public class JUnitTestClassLevelTransformer
		implements ClassFileTransformer, Opcodes
{

	private static Logger logger = Logger
			.getLogger(JUnitTestClassLevelTransformer.class);
	// handle junit4 test class runnere
	public static final String PARENT_RUNNER_CLASS = "org/junit/runners/ParentRunner";
	public static final String PARENT_RUNNER_CLASS_DOT = "org.junit.runners.ParentRunner";
	// handle junit3 test class runner
	public static final String JUNIT38_RUNNER_CLASS = "junit/framework/TestSuite";
	public static final String JUNIT38_RUNNER_CLASS_DOT = "junit.framework.TestSuite";

	public static final String JUNIT4_PREFIX = "org/junit/";
	public static final String JUNIT3_PREFIX = "junit/framework/";
	// public static final String JUNIT38_RUNNER_CLASS =
	// "junit/framework/TestCase";

	public static final String RUN_METH = "run";
	public static final String INIT = "<init>";
	public static final String RUN4CLASS = "runnerForClass";
	public static final String RUN4CLASS_DESC = "(Ljava/lang/Class;)Lorg/junit/runner/Runner;";

	public static final String JUNIT_NOTIFIER_CLASS = "org/junit/runner/notification/RunNotifier";

	public static String className;

	public byte[] transform(ClassLoader loader, String className,
			Class<?> classBeingRedefined, ProtectionDomain protectionDomain,
			byte[] classfileBuffer) throws IllegalClassFormatException {
		try {
			if (className == null) {
				return classfileBuffer;
			}

			if (!className.equals(JUNIT38_RUNNER_CLASS)
					&& !className.startsWith(JUNIT4_PREFIX))
				return classfileBuffer;

			logger.debug("transforming JUnit class: " + className);
			this.className = className;

			byte[] result = classfileBuffer;
			ClassReader reader = new ClassReader(classfileBuffer);
			ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_FRAMES);

			ClassVisitor cv = new ClassVisitor(ASM5, writer) {
				@Override
				public MethodVisitor visitMethod(final int access,
						final String name, final String desc,
						final String signature, final String[] exceptions) {
					MethodVisitor mv = cv.visitMethod(access, name, desc,
							signature, exceptions);
					// transform JUnit4 to dump coverage
					if (matchJUnit4Runner(JUnitTestClassLevelTransformer.className)) {
						return new JUnit4MethodVisitor(mv,JUnitTestClassLevelTransformer.className);
					}

					// transform JUnit3 to dump coverage
					else if (matchJUnit3Suite(
							JUnitTestClassLevelTransformer.className, name,desc)) {
						FTracerLogger.debug("TestClassTransformer "+ JUnitTestClassLevelTransformer.className+"-"+name);
						return new JUnit3MethodVisitor(mv);
					}
					/*
					 * // collect the coverage info else if
					 * (JUnitTestClassLevelTransformer.className
					 * .equals(JUNIT_NOTIFIER_CLASS) &&
					 * (name.equals("fireTestRunStarted") ||
					 * name.equals("fireTestRunFinished"))) return new
					 * JUnitTestRunStartEndEventMethodVisitor(mv, name);
					 */
					else
						return mv;
				}
			};
			reader.accept(cv, ClassReader.EXPAND_FRAMES);
			result = writer.toByteArray();
			return result;
		} catch (Throwable t) {
			t.printStackTrace();
			String message = "Exception thrown during instrumentation";
			logger.error(message, t);
			System.err.println(message);
			System.exit(1);
		}
		throw new RuntimeException("Should not be reached");
	}

	public static boolean matchJUnit3Suite(String className, String methName, String desc) {
		return JUNIT38_RUNNER_CLASS.equals(className) &&INIT.equals(methName)&desc.equals("(Ljava/lang/Class;)V");
	}

	public static boolean matchJUnit4Runner(String className) {
		return className.startsWith(JUNIT4_PREFIX);
	}

//	public static boolean matchJUnit3RunDotName(String className,
//			String methName) {
//		return JUNIT38_RUNNER_CLASS_DOT.equals(className)
//				&& RUN_METH.equals(methName);
//	}
//
//	public static boolean matchJUnit4RunDotName(String className,
//			String methName) {
//		return PARENT_RUNNER_CLASS_DOT.equals(className)
//				&& RUN_METH.equals(methName);
//	}

}
