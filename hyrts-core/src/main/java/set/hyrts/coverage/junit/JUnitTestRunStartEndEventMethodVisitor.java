/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.coverage.junit;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import set.hyrts.coverage.io.TracerIO;
import set.hyrts.rts.HybridRTS;
import set.hyrts.rts.HybridRTSWithBlock;
import set.hyrts.rts.MethRTS;
import set.hyrts.utils.Properties;
import set.hyrts.utils.Properties.RTSVariant;

/**
 * Deprecated since now using surefire to exclude tests rather than directly using JUnit
 * @author lingmingzhang
 *
 */
@Deprecated
class JUnitTestRunStartEndEventMethodVisitor extends MethodVisitor
		implements Opcodes
{
	String mName;

	public JUnitTestRunStartEndEventMethodVisitor(final MethodVisitor mv,
			String name) {
		super(ASM5, mv);
		this.mName = name;
	}

	@Override
	public void visitCode() {
		// if this is the test run start, check for test exclusion
		if (mName.equals("fireTestRunStarted")) {
			// if RTS, then diff the two versions to compute affected tests
			if (Properties.RTS == RTSVariant.FRTS
					|| Properties.RTS == RTSVariant.HyRTS
					|| Properties.RTS == RTSVariant.HyRTSf) {
				mv.visitMethodInsn(INVOKESTATIC, HybridRTS.CLASS, "main", "()V",
						false);
			} else if (Properties.RTS == RTSVariant.HyRTSb) {
				mv.visitMethodInsn(INVOKESTATIC, HybridRTSWithBlock.CLASS,
						"main", "()V", false);
			} else if (Properties.RTS == RTSVariant.MRTS) {
				mv.visitMethodInsn(INVOKESTATIC, MethRTS.CLASS, "main", "()V",
						false);
			}
		} else {
			// if this is the test run end, clean obsolete test cov and check
			// for consistency
			mv.visitMethodInsn(INVOKESTATIC, TracerIO.CLASS,
					"cleanObsoleteTestCov", "()V", false);
		}
		mv.visitCode();
	}
}