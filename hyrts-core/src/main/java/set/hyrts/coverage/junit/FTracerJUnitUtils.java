/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.coverage.junit;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runners.model.RunnerBuilder;


import junit.framework.TestSuite;
import set.hyrts.coverage.core.CoverageData;
import set.hyrts.coverage.io.TracerIO;
import set.hyrts.coverage.junit.runners.FTracerJUnit3Runner;
import set.hyrts.coverage.junit.runners.FTracerJUnit4Runner;
import set.hyrts.logger.FTracerLogger;
import set.hyrts.utils.Properties;

public class FTracerJUnitUtils
{
	static final String className = "set/hyrts/coverage/junit/FTracerJUnitUtils";
	static final String transJUnit3SuiteInit = "transformJUnit3SuitInit";
	static final String transJUnit4Runner4Class = "transformJUnit4Runner4Class";
	static final String isExcluded = "isExcluded";
	static final String dumpCov = "dumpCoverage";
	public static Set<String> allTests = new HashSet<String>();
	public static Set<String> runnedTests = new HashSet<String>();
	public static Set<String> excluded = new HashSet<String>();

	/*
	public static boolean isExcluded(String testClass) throws IOException {
		// if testClass is abstract higher level runner, should check it;
		// otherwise no lower-level (such as parameterized tests) test will be
		// checked
		if (testClass == null || isParameterizedTest(testClass))
			return false;
		// first store the test class
		allTests.add(testClass);
		// second check if it should be excluded
		boolean isExcluded = excluded.contains(testClass);
		if (isExcluded) {
			File oldCovFile = new File(Properties.OLD_DIR + File.separator
					+ Properties.TRACER_ROOT_DIR + File.separator
					+ Properties.TRACER_COV_TYPE + File.separator + testClass
					+ Properties.GZ_EXT);
			File newCovFile = new File(Properties.TRACER_ROOT_DIR
					+ File.separator + Properties.TRACER_COV_TYPE
					+ File.separator + testClass + Properties.GZ_EXT);
			newCovFile.getParentFile().mkdirs();
			// bug fix: changing from getAbsolutePath() into getCanonicalPath()
			// to remove redundant names such as "."
			if (!oldCovFile.getCanonicalPath()
					.equals(newCovFile.getCanonicalPath())) {
				Files.copy(oldCovFile, newCovFile);
			}
		} else {
			runnedTests.add(testClass);
		}
		return isExcluded;
	}*/

	public static boolean transformJUnit3SuitInit(TestSuite suite,
			Class testClass) {
		if (checkStackForJUnit(FTracerJUnit3Runner.class.getName()) == 0
				&& checkStackForJUnit("org.junit.internal.builders.") == 0) {
			// check for JUnit 3 test exclusion
			if (FTracerJUnit3Runner.JUnit3Excluded != null
					&& FTracerJUnit3Runner.JUnit3Excluded
							.contains(testClass.getCanonicalName()))
				return false;
			suite.addTest(new FTracerJUnit3Runner(testClass));
			return false;
		} else
			return true;
	}

	public static Runner transformJUnit4Runner4Class(RunnerBuilder builder,
			Class testClass, String runnerClass) throws Throwable {
		// check if runnerForClass has already been transformed
		if (checkStackForJUnit(FTracerJUnitUtils.class.getName()) > 2) {
			return builder.runnerForClass(testClass);
		} else {
			Runner runner = builder.runnerForClass(testClass);
			return new FTracerJUnit4Runner(runner, testClass);
		}
	}

	public static void dumpCoverage(Description d) throws IOException {
		String test = d.getClassName() + "." + d.getMethodName();
		dumpCoverage(test);
	}

	public static void dumpCoverage(Class<?> testClass) throws IOException {
		String test = testClass.getCanonicalName();
		dumpCoverage(test);
	}

	private static void dumpCoverage(String test) throws IOException {
		if (Properties.EXECUTION_ONLY || test == null
				|| FTracerJUnitUtils.excluded.contains(test))
			return;
		String fileName = TracerIO.getTestCovFilePath(Properties.NEW_DIR, test);
		if (Properties.TRACER_COV_TYPE.endsWith(Properties.METH_COV)) {
			if (!Properties.TRACE_RTINFO)
				TracerIO.writeMethodCov(test, fileName);
			else
				TracerIO.writeMethodCovWithRT(test, fileName);
		} else if (Properties.TRACER_COV_TYPE.endsWith(Properties.CLASS_COV))
			TracerIO.writeClassCov(test, fileName);
		else if (Properties.TRACER_COV_TYPE.endsWith(Properties.STMT_COV))
			TracerIO.writeStmtCov(test, fileName);
		else if (Properties.TRACER_COV_TYPE.endsWith(Properties.BRANCH_COV))
			TracerIO.writeBranchCov(test, fileName);
		else if (Properties.TRACER_COV_TYPE.endsWith(Properties.BLK_COV))
			TracerIO.writeBlockCovWithRT(test, fileName);
	}

	public static String getFileName(String test) {
		if (Properties.TC_LEVEL.equals(Properties.TEST_LEVEL))
			return test;
		else
			return org.apache.commons.codec.digest.DigestUtils.sha1Hex(test);
	}

	public static boolean isParameterizedTest(String testClass) {
		return testClass.startsWith("[");
	}

	private static int checkStackForJUnit(String clazz) {
		StackTraceElement[] arrayOfStackTraceElement1 = Thread.currentThread()
				.getStackTrace();
		int i = 0;
		for (StackTraceElement localStackTraceElement : arrayOfStackTraceElement1) {
			// System.out.print(localStackTraceElement.getClassName()+":
			// "+localStackTraceElement.getMethodName()+" || ");
			if (localStackTraceElement.getClassName().startsWith(clazz)) {
				i++;
			}
		}
		// System.out.println();
		return i;
	}

}
