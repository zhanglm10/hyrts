/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.coverage.junit;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import set.hyrts.utils.Properties;

import org.junit.runner.Runner;
class JUnit4MethodVisitor extends MethodVisitor implements Opcodes
{
	public String className;

	public JUnit4MethodVisitor(final MethodVisitor mv, String className) {
		super(ASM5, mv);
		this.className=className;
	}

	/*@Override
	public void visitCode() {
		// if the test is excluded, then directly return without execution
		mv.visitVarInsn(ALOAD, 0);
		mv.visitMethodInsn(INVOKEVIRTUAL, "org/junit/runners/ParentRunner",
				"getName", "()Ljava/lang/String;", false);
		mv.visitMethodInsn(INVOKESTATIC, FTracerJUnitRunner.className,
				FTracerJUnitRunner.isExcluded, "(Ljava/lang/String;)Z", false);
		Label label = new Label();
		mv.visitJumpInsn(IFEQ, label);
		mv.visitInsn(RETURN);
		mv.visitLabel(label);
		// mv.visitInsn();
		mv.visitCode();
	}*/

	// transform runnerForClass to dump coverage
	@Override
	public void visitMethodInsn(int opcode, String owner, String name,
			String desc, boolean itf) {
		if (name.equals("runnerForClass") && desc
				.equals("(Ljava/lang/Class;)Lorg/junit/runner/Runner;")) {
			mv.visitLdcInsn(className);
			mv.visitMethodInsn(INVOKESTATIC, FTracerJUnitUtils.className,
					FTracerJUnitUtils.transJUnit4Runner4Class,
					"(Lorg/junit/runners/model/RunnerBuilder;Ljava/lang/Class;Ljava/lang/String;)Lorg/junit/runner/Runner;",
					itf);
		} else
			mv.visitMethodInsn(opcode, owner, name, desc, itf);
		/*if (name.equals("evaluate")) {
			mv.visitVarInsn(ALOAD, 0);
			mv.visitMethodInsn(INVOKEVIRTUAL, "org/junit/runners/ParentRunner",
					"getName", "()Ljava/lang/String;", itf);
		
			mv.visitMethodInsn(INVOKESTATIC, FTracerJUnitRunner.className,
					FTracerJUnitRunner.transJUnit4Runner,
					"(Ljava/lang/String;)V", itf);
		}*/
	}

}