/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.coverage.junit;

import java.io.File;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.log4j.Logger;
import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.RunListener;

import set.hyrts.coverage.io.TracerIO;
import set.hyrts.rts.HybridRTS;
import set.hyrts.utils.Properties;

@Deprecated
public class JUnitTestMethodListener extends RunListener
{
	static int test = 0;
	private static Logger logger = Logger.getLogger(JUnitTestMethodListener.class);
	public String lastTestClass = "";

	public static AtomicReference<ConcurrentMap<String, Integer>> coverage_map = new AtomicReference<ConcurrentMap<String, Integer>>(
			new ConcurrentHashMap<String, Integer>());

	public static ConcurrentMap<String, Integer> getCoverage() {
		return coverage_map.get();
	}

	public JUnitTestMethodListener() {
	}

	public void testRunStarted(Description description) throws Exception {
		// Runtime.getRuntime().addShutdownHook(new Thread() {
		// public void run() {
		// System.out.println("Running Shutdown Hook");
		// for(int a: CoverageData.idClassMap.keySet()){
		// System.out.println(CoverageData.covArray.size());
		// }
		// }
		// });
		//Tool_TC_MethLevelRTS.main(Properties.OLD_DIR, Properties.NEW_CLASSPATH);
	}

	public void testRunFinished(Result result) throws Exception {
		System.out.println(">>Number of tests executed: " + result.getRunCount());
	}

	public void testStarted(Description description) throws Exception {
		// System.out.println("Starting: " + description.getMethodName());
		// coverage_map.get().clear();
	}

	public void testFinished(Description description) throws Exception {
		String testName = getTestName(description);
		System.out.println("Listening "+testName);
		/*
		 * // System.out.println(testName + ": " + CoverageData.covArray.size() +
		// "|"
		// + CoverageData.idClassMap.size() + "|"
		// + CoverageData.idMethMap.size());
		Tracer.getInstance().deactivateTrace();

		// coverage_map.set(new ConcurrentHashMap<String, Integer>());
		String dirName=TracerIO.getCovDir();
		if (!CoverageData.WITH_RTINFO)
			TracerIO.writeTrace(testName, dirName + File.separator
					+ getFileName(testName) + Properties.GZ_EXT);
		else
			TracerIO.writeTraceWithRT(testName, dirName + File.separator
					+ getFileName(testName) + Properties.GZ_EXT);
		Tracer.getInstance().activateTrace();*/
	}

	

	public String getFileName(String test) {
		return org.apache.commons.codec.digest.DigestUtils.sha1Hex(test);
	}

	private static String getTestName(Description d) {
		return d.getClassName() + "." + d.getMethodName();
	}

	private static String getTestClassName(Description d) {
		String testMethod = d.getClassName() + "." + d.getMethodName();
		return testMethod.substring(0, testMethod.lastIndexOf("."));
	}
}
