package set.hyrts.coverage.junit.runners;

import java.io.IOException;
import java.util.Set;

import junit.framework.Test;
import junit.framework.TestResult;
import junit.framework.TestSuite;
import set.hyrts.coverage.junit.FTracerJUnitUtils;

public class FTracerJUnit3Runner implements Test
{
	private TestSuite suite;
	private Class testClass;
	public static Set<String> JUnit3Excluded;

	public FTracerJUnit3Runner(Class testClass) {
		this.testClass = testClass;
		suite = new TestSuite(testClass);
	}

	@Override
	public int countTestCases() {
		return suite != null ? suite.countTestCases() : 0;
	}

	@Override
	public void run(TestResult result) {
		suite.run(result);
		try {
			FTracerJUnitUtils.dumpCoverage(testClass);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
