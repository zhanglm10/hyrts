/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.coverage.agent;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.security.ProtectionDomain;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;

import set.hyrts.coverage.core.ClassTransformVisitor;
import set.hyrts.coverage.core.CoverageData;
import set.hyrts.utils.Classes;
import set.hyrts.utils.Properties;

public class ClassTransformer implements ClassFileTransformer
{
	static Set<String> excludedPrefixes = new HashSet<String>();
	static {
		excludedPrefixes.add("org/junit"); // exclude junit
		excludedPrefixes.add("junit/"); // exclude junit
		excludedPrefixes.add("org/apache/maven"); // exclude build system
		excludedPrefixes.add("set/hyrts"); // exclude itself and used libs
		excludedPrefixes.add("org/objectweb"); // exclude asm
		excludedPrefixes.add("java/");
		excludedPrefixes.add("javax/");
		excludedPrefixes.add("sun/");
		excludedPrefixes.add("com/sun/");

	}

	private static Logger logger = Logger.getLogger(ClassTransformer.class);

	public byte[] transform(ClassLoader loader, String slashClassName,
			Class<?> classBeingRedefined, ProtectionDomain protectionDomain,
			byte[] classfileBuffer) throws IllegalClassFormatException {
		try {
			if (slashClassName == null) {
				return classfileBuffer;
			}
			if (loader != ClassLoader.getSystemClassLoader()) {
				return classfileBuffer;
			}

			if (isExcluded(loader, slashClassName)) {
				return classfileBuffer;
			}
			logger.debug("transforming: " + slashClassName);

			String dotClassName = Classes.toDotClassName(slashClassName);
			int clazzId = CoverageData.registerClass(slashClassName,
					dotClassName);
			byte[] result = classfileBuffer;
			ClassReader reader = new ClassReader(classfileBuffer);
			// ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
			// using the following code to fix the issue for linkage errors.
			// TODO: 0.3s slower for joda-time
			ClassWriter writer = new ComputeClassWriter(
					FrameOptions.pickFlags(classfileBuffer));
			ClassTransformVisitor cv = new ClassTransformVisitor(clazzId,
					slashClassName, dotClassName, writer);
			reader.accept(cv, ClassReader.EXPAND_FRAMES);
			result = writer.toByteArray();
			return result;
		} catch (Throwable t) {
			t.printStackTrace();
			String message = "Exception thrown during instrumentation";
			logger.error(message, t);
			System.err.println(message);
			System.exit(1);
		}
		throw new RuntimeException("Should not be reached");
	}

	/**
	 * Check if the class should be excluded
	 * 
	 * @param className
	 * @return
	 */
	public static boolean isExcluded(String className) {
		/*if (!Properties.TRACE_LIB) {
			if (Properties.PROJECT_PREFIX!=null&&!className.startsWith(Properties.PROJECT_PREFIX)) {
				return true;
			}
		} else*/
		{
			for (String prefix : excludedPrefixes) {
				if (className.startsWith(prefix))
					return true;
			}
		}
		return false;
	}

	public static boolean isExcluded(ClassLoader loader, String className) {
		for (String prefix : excludedPrefixes) {
			if (className.startsWith(prefix))
				return true;
		}

		if (!Properties.TRACE_LIB) {
			URL url = loader.getResource(className + ".class");
			// exclude all 3rd party and jdk libs
			if (url == null || url.getProtocol().equals("jar")) {
				return true;
			}
		}
		return false;

	}

}
