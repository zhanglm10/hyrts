/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.coverage.agent;
import org.objectweb.asm.ClassWriter;

/**
 *  Class for compute ASM frame options
 *  @author Henry Coles
 */
public class FrameOptions {

  private static final int JAVA_7 = 51;

  /**
   * Java 7 and above require frame info for class version above 7. The ASM compute
   * frame options does not support the JSR opcode used by some pre 7 compilers
   * when generating java 5 bytecode.
   * 
   * We dodge this issue by only computing frames for classes with a version
   * above 6.
   * 
   * @param bs a class
   * @return appropriate flags
   */
  public static int pickFlags(byte[] bs) {
    if ( needsFrames(bs) ) {
      return ClassWriter.COMPUTE_FRAMES;
    }
    return ClassWriter.COMPUTE_MAXS;
  }
  
  public static boolean needsFrames(byte[] bs) {
    short majorVersion =(short)( ((bs[6]&0xFF)<<8) | (bs[7]&0xFF) );
    return majorVersion >= JAVA_7;
  }
  
}
