/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.coverage.agent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import set.hyrts.coverage.junit.JUnitTestClassLevelTransformer;
import set.hyrts.coverage.junit.JUnitTestMethodLevelTransformer;
import set.hyrts.coverage.junit.runners.FTracerJUnit3Runner;
import set.hyrts.coverage.maven.SurefireTransformer;
import set.hyrts.utils.Properties;

public class JUnitAgent
{
	private static Logger logger = Logger.getLogger(JUnitAgent.class);

	/**
	 * Premain for starting with surefire test execution to capture dynamic
	 * dependency information
	 * 
	 * @param args
	 * @param inst
	 * @throws Exception
	 */
	public static void premain(String args, Instrumentation inst)
			throws Exception {
		logger.debug("premain executed: " + args);
		Properties.processPreMainArguments(args);
		if (Properties.TRACER_COV_TYPE != null) {
			if (!Properties.EXECUTION_ONLY)
				inst.addTransformer(new ClassTransformer());
			if (Properties.TM_LEVEL.equals(Properties.TEST_LEVEL))
				inst.addTransformer(new JUnitTestMethodLevelTransformer());
			else
				inst.addTransformer(new JUnitTestClassLevelTransformer());
		}
		updateJUnit3Excludes();
	}

	/**
	 * Agentmain for capturing surefire events to exclude tests
	 * 
	 * @param args
	 * @param inst
	 */
	public static void agentmain(String args, Instrumentation inst) {
		logger.debug("agentmain executed");
		Properties.processAgentMainArguments(args);

		inst.addTransformer(new SurefireTransformer(), true);
		try {
			for (Class localClass : inst.getAllLoadedClasses()) {
				String str = localClass.getName();
				if ((str.equals(
						"org.apache.maven.plugin.surefire.AbstractSurefireMojo"))
						|| (str.equals(
								"org/apache/maven/plugin/surefire/SurefirePlugin"))
						|| (str.equals(
								"org/apache/maven/plugin/failsafe/IntegrationTestMojo"))
						|| (str.equals("org/scalatest/tools/maven/TestMojo"))) {
					inst.retransformClasses(localClass);
				}
			}
		} catch (UnmodifiableClassException localUnmodifiableClassException) {
		}
	}

	public static void updateJUnit3Excludes() throws IOException {
		String path = getJUnit3ExcludePath();
		File f = new File(path);
		if (!f.exists())
			return;
		Set<String> excluded = new HashSet<String>();
		BufferedReader reader = new BufferedReader(new FileReader(path));
		String line = reader.readLine();
		while (line != null) {
			excluded.add(line);
			line = reader.readLine();
		}
		reader.close();
		FTracerJUnit3Runner.JUnit3Excluded = excluded;
		f.delete();
	}
	public static String getJUnit3ExcludePath() {
		String dir = Properties.NEW_DIR + File.separator
				+ Properties.TRACER_ROOT_DIR;
		File file = new File(dir);
		file.mkdirs();
		return dir + File.separator + Properties.EXCLUDE;
	}
}
