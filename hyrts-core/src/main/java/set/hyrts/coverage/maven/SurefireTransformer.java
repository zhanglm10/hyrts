package set.hyrts.coverage.maven;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

import org.apache.log4j.Logger;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;

import set.hyrts.coverage.agent.JUnitAgent;

public class SurefireTransformer implements ClassFileTransformer, Opcodes
{
	private static Logger logger = Logger.getLogger(SurefireTransformer.class);

	@Override
	public byte[] transform(ClassLoader loader, String className,
			Class<?> classBeingRedefined, ProtectionDomain protectionDomain,
			byte[] classfileBuffer) throws IllegalClassFormatException {
		if (className == null) {
			return classfileBuffer;
		}
		/*if (loader != ClassLoader.getSystemClassLoader()) {
			return classfileBuffer;
		}*/
		if ((className
				.equals("org/apache/maven/plugin/surefire/AbstractSurefireMojo"))
				|| (className
						.equals("org/apache/maven/plugin/surefire/SurefirePlugin"))
				|| (className.equals(
						"org/apache/maven/plugin/failsafe/IntegrationTestMojo"))) {
			ClassReader localClassReader = new ClassReader(classfileBuffer);
			ClassWriter localClassWriter = new ClassWriter(localClassReader,
					ClassWriter.COMPUTE_FRAMES);
			SurefireClassVisitor localMavenClassVisitor = new SurefireClassVisitor(
					localClassWriter);
			localClassReader.accept(localMavenClassVisitor,
					ClassReader.EXPAND_FRAMES);
			return localClassWriter.toByteArray();
			// return addInterceptor(className, classfileBuffer,
			// "org/ekstazi/maven/SurefireMojoInterceptor");
		}
		return null;
	}

}
