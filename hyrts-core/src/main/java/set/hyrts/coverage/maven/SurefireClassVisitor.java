package set.hyrts.coverage.maven;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class SurefireClassVisitor extends ClassVisitor implements Opcodes
{
	public SurefireClassVisitor(ClassVisitor cv) {
		super(ASM5, cv);
	}

	@Override
	public MethodVisitor visitMethod(final int access, final String name,
			final String desc, final String signature,
			final String[] exceptions) {
		MethodVisitor mv = this.cv.visitMethod(access, name, desc, signature,
				exceptions);
		if ((name.equals("execute")) && (desc.equals("()V"))) {
			mv = new SurefireMethodVisitor(mv, name, desc);
		}
		return mv;
	}
}

class SurefireMethodVisitor extends MethodVisitor implements Opcodes
{
	String methName;
	String desc;

	public SurefireMethodVisitor(MethodVisitor mv, String name, String desc) {
		super(ASM5, mv);
		this.methName = name;
		this.desc = desc;
	}

	@Override
	public void visitCode() {
		mv.visitVarInsn(ALOAD, 0);
		mv.visitMethodInsn(INVOKESTATIC, SurefireRewriter.className, methName,
				desc.replace("(", "(Ljava/lang/Object;"), false);
		// mv.visitInsn();
		mv.visitCode();
	}
}
