/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.coverage.core;

import java.util.HashSet;
import java.util.Set;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

class MethodVisitorBranchCov extends MethodVisitor implements Opcodes
{

	int clazzId;
	int methId;
	String clazzName;
	String methName;
	static int branchID = 0;// Global branch id
	static int switchLabelID = 0;// Global switch label id
	static int lineNum = -1;
	static Set<Label> switchLabels = new HashSet<Label>();

	public MethodVisitorBranchCov(final MethodVisitor mv, int clazzId,
			int methId, String clazzName, String methName, boolean isInit,
			int access) {
		super(ASM5, mv);
		this.clazzId = clazzId;
		this.methId = methId;
		this.clazzName = clazzName;
		this.methName = methName;
	}

	@Override
	public void visitCode() {
		mv.visitCode();
		switchLabels.clear();
	}

	@Override
	public void visitJumpInsn(int type, Label target) {
		if (type == org.objectweb.asm.Opcodes.GOTO) {
			mv.visitJumpInsn(type, target);
			return;
		}
		int curBranch=nextBranchID();

		mv.visitLdcInsn("false");
		mv.visitLdcInsn(clazzName + ":" + lineNum + ":" + curBranch + "");
		mv.visitMethodInsn(Opcodes.INVOKESTATIC, CoverageData.TRACER,
				Tracer.TRACE_BRANCH_COV,
				"(Ljava/lang/String;Ljava/lang/String;)V", false);
		mv.visitJumpInsn(type, target);

		mv.visitLdcInsn("true");
		mv.visitLdcInsn(clazzName + ":" + lineNum + ":" + curBranch + "");
		mv.visitMethodInsn(Opcodes.INVOKESTATIC, CoverageData.TRACER,
				Tracer.TRACE_BRANCH_COV,
				"(Ljava/lang/String;Ljava/lang/String;)V", false);
	}

	@Override
	public void visitTableSwitchInsn(int type, int arg2, Label defaultTarget,
			Label[] larray) {
		for (Label l : larray)
			switchLabels.add(l);
		switchLabels.remove(defaultTarget);
		mv.visitTableSwitchInsn(type, arg2, defaultTarget, larray);
	}

	@Override
	public void visitLookupSwitchInsn(Label defaultTarget, int[] iarray,
			Label[] larray) {
		for (Label l : larray)
			switchLabels.add(l);
		switchLabels.remove(defaultTarget);
		mv.visitLookupSwitchInsn(defaultTarget, iarray, larray);
	}

	@Override
	public void visitLabel(Label l) {

		mv.visitLabel(l);
		if (switchLabels.contains(l)) {
			int curSwitch=nextSwitchID();
			mv.visitLdcInsn("");
			mv.visitLdcInsn(
					clazzName + ":" + (lineNum + 1) + ":" + curSwitch);
			mv.visitMethodInsn(Opcodes.INVOKESTATIC, CoverageData.TRACER,
					Tracer.TRACE_BRANCH_COV,
					"(Ljava/lang/String;Ljava/lang/String;)V", false);
		}
	}
	

	@Override
	public void visitLineNumber(int line, Label start) {
		mv.visitLineNumber(line, start);
		lineNum = line;
	}

	private synchronized static int nextBranchID() {
		return branchID++;
	}

	private synchronized static int nextSwitchID() {
		return switchLabelID++;
	}
}