/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.coverage.core;

import org.apache.log4j.Logger;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import set.hyrts.coverage.agent.ClassTransformer;
import set.hyrts.utils.Classes;
import set.hyrts.utils.Properties;

class MethodVisitorBlockCov extends MethodVisitor implements Opcodes
{

	private static Logger logger = Logger
			.getLogger(MethodVisitorBlockCov.class);

	int clazzId;
	int methId;
	String slashClazzName;
	String dotClazzName;
	String methName;
	boolean isVirtual;
	boolean isInit;
	int labelId = 0;

	public MethodVisitorBlockCov(final MethodVisitor mv, int clazzId,
			int methId, String slashClazzName, String dotClazzName,
			String methName, boolean isInit, int access) {
		super(ASM5, mv);
		this.clazzId = clazzId;
		this.methId = methId;
		this.slashClazzName = slashClazzName;
		this.dotClazzName = dotClazzName;
		this.methName = methName;
		this.isInit = isInit;
		// logger.info(clazzName+":"+methName);
	}

	@Override
	public void visitLabel(Label label) {
		super.visitLabel(label);
		mv.visitLdcInsn(clazzId);
		mv.visitLdcInsn(methId);
		// logger.info(labelId);
		mv.visitLdcInsn(labelId++);

		mv.visitMethodInsn(INVOKESTATIC, CoverageData.TRACER,
				Tracer.TRACE_BLOCK_COV, "(III)V", false);
	}

	// trace the method invocations
	@Override
	public void visitCode() {
		// method coverage
		mv.visitLdcInsn(clazzId);
		mv.visitLdcInsn(methId);
		mv.visitMethodInsn(INVOKESTATIC, CoverageData.TRACER,
				Tracer.TRACE_METH_COV, "(II)V", false);

		// runtime type info
		if (Properties.TRACE_RTINFO && isVirtual && !isInit) {
			mv.visitLdcInsn(clazzId);
			mv.visitLdcInsn(methId);
			mv.visitLdcInsn(dotClazzName);
			mv.visitLdcInsn(methName);

			mv.visitVarInsn(ALOAD, 0);
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "getClass",
					"()Ljava/lang/Class;", false);
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "getName",
					"()Ljava/lang/String;", false);
			mv.visitMethodInsn(INVOKESTATIC, CoverageData.TRACER,
					Tracer.TRACE_METH_COV_RT,
					"(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V",
					false);
		}
		super.visitCode();
	}

	@Override
	public void visitLdcInsn(Object o) {
		if ((o instanceof Type)) {
			Type t = (Type) o;
			if (t != null && t.getSort() == 10) {
				String str = Classes.descToClassName(t.getDescriptor());
				if (!str.equals(this.slashClazzName)
						&& !ClassTransformer.isExcluded(str)) {
					mv.visitLdcInsn(t);
					mv.visitMethodInsn(Opcodes.INVOKESTATIC,
							CoverageData.TRACER, Tracer.TRACE_CLINIT,
							"(Ljava/lang/Class;)V", false);
				}
			}
		}
		this.mv.visitLdcInsn(o);
	}

	@Override
	public void visitMethodInsn(int opcode, String owner, String name,
			String desc, boolean itf) {
		mv.visitMethodInsn(opcode, owner, name, desc, itf);
		// System.out.println(owner);
		if (opcode == Opcodes.INVOKEINTERFACE
				&& !ClassTransformer.isExcluded(owner)) {
			// System.out.println("{"+owner);
			// String dotClazzName = CoverageData.classNameMap.get(owner);
			//String dotClazzName = Classes.toDotClassName(owner);
			// adding more delicated checks to avoid adding unnecessary bytecode
			// if (CoverageData.classIdMap.containsKey(dotClazzName)) {
			// System.out.println("{{"+dotClazzName+"\t"+this.dotClazzName+"-"+this.methName);
			Integer classId = CoverageData.classIdMap.get(owner);
			if (classId != null) {
				mv.visitLdcInsn(classId.intValue());
				mv.visitMethodInsn(Opcodes.INVOKESTATIC, CoverageData.TRACER,
						Tracer.TRACE_CLINIT, "(I)V", false);
			} else {
				mv.visitLdcInsn(owner);
				mv.visitMethodInsn(Opcodes.INVOKESTATIC, CoverageData.TRACER,
						Tracer.TRACE_CLINIT, "(Ljava/lang/String;)V", false);
			}
		}
	}

	@Override
	public void visitFieldInsn(int opcode, String owner, String name,
			String desc) {
		mv.visitFieldInsn(opcode, owner, name, desc);
		if (/* opcode==Opcodes.PUTSTATIC|| */ opcode == Opcodes.GETSTATIC) {
			if (!owner.equals(this.slashClazzName)) {
				mv.visitLdcInsn(owner);
				//TODO: whether to ignore this
				mv.visitMethodInsn(Opcodes.INVOKESTATIC, CoverageData.TRACER,
						Tracer.TRACE_CLINIT, "(Ljava/lang/String;)V", false);
			}
			if (!Classes.isPrimitive(desc)) {
				String fieldType = Classes.descToClassName(desc);
				if (!fieldType.equals(this.slashClazzName)
						&& !ClassTransformer.isExcluded(fieldType)) {
					// record runtime type for static fields
					mv.visitFieldInsn(Opcodes.GETSTATIC, owner, name, desc);
					mv.visitMethodInsn(Opcodes.INVOKESTATIC,
							CoverageData.TRACER, Tracer.TRACE_CLINIT,
							"(Ljava/lang/Object;)V", false);
				}
			}
		}
	}

	public void visitMaxs(int maxStack, int maxLocals) {
		mv.visitMaxs(maxStack + 4, maxLocals);
	}

	public boolean isVirtual(int access) {
		return 0 == (access & Opcodes.ACC_STATIC);
	}

}