/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.coverage.core;

import java.util.BitSet;
import java.util.HashSet;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import set.hyrts.utils.Properties;

public class ClassTransformVisitor extends ClassVisitor implements Opcodes
{
	int clazzId;
	String slashClazzName;
	String dotClazzName;

	public ClassTransformVisitor(int clazzId, String clazzName,
			String dotClassName, final ClassVisitor cv) {
		super(ASM5, cv);
		this.clazzId = clazzId;
		this.slashClazzName = clazzName;
		this.dotClazzName = dotClassName;
		CoverageData.registerMeth(clazzId, CoverageData.CLINIT);
	}

	@Override
	public MethodVisitor visitMethod(final int access, final String name,
			final String desc, final String signature,
			final String[] exceptions) {
		String methName = name + ":" + desc;
		int methId = CoverageData.registerMeth(clazzId, methName);
		boolean isInit = name.startsWith("<init>");
		MethodVisitor mv = cv.visitMethod(access, name, desc, signature,
				exceptions);
		if (Properties.TRACER_COV_TYPE.endsWith(Properties.STMT_COV))
			return mv == null ? null
					: new MethodVisitorStmtCov(mv, clazzId, methId, isInit,
							access);
		if (Properties.TRACER_COV_TYPE.endsWith(Properties.BRANCH_COV))
			return mv == null ? null
					: new MethodVisitorBranchCov(mv, clazzId, methId,
							slashClazzName, methName, isInit, access);
		if (Properties.TRACER_COV_TYPE.endsWith(Properties.METH_COV))
			return mv == null ? null
					: new MethodVisitorMethCov(mv, clazzId, methId,
							slashClazzName, dotClazzName, methName, isInit,
							access);
		if (Properties.TRACER_COV_TYPE.endsWith(Properties.CLASS_COV))
			return mv == null ? null
					: new MethodVisitorClassCov(mv, clazzId, methId,
							slashClazzName, dotClazzName, methName, isInit,
							access);
		if (Properties.TRACER_COV_TYPE.endsWith(Properties.BLK_COV))
			return mv == null ? null
					: new MethodVisitorBlockCov(mv, clazzId, methId,
							slashClazzName, dotClazzName, methName, isInit,
							access);
		return mv;
	}

	@Override
	public void visitEnd() {
		super.visitEnd();
		
		int methNum = 1;
		if (Properties.TRACER_COV_TYPE.endsWith(Properties.METH_COV)) {
			// bug fix: add check for the case that no method has been
			// registered
			if (CoverageData.idMethMap.containsKey(clazzId))
				methNum = CoverageData.idMethMap.get(clazzId).size();
			CoverageData.methCovArray[clazzId] = new boolean[methNum];

		} else if (Properties.TRACER_COV_TYPE.endsWith(Properties.BLK_COV)) {
			if (CoverageData.idMethMap.containsKey(clazzId)) {
				methNum = CoverageData.idMethMap.get(clazzId).size();
			}
			CoverageData.methCovArray[clazzId] = new boolean[methNum];
			CoverageData.blockCovSet[clazzId] = new BitSet[methNum];
			for (int i = 0; i < CoverageData.blockCovSet[clazzId].length; i++)
				CoverageData.blockCovSet[clazzId][i] = new BitSet();
		} else if (Properties.TRACER_COV_TYPE.endsWith(Properties.STMT_COV)) {
			CoverageData.stmtCovSet[clazzId] = new BitSet();
		}
	}

}
