/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.coverage.core;

import java.util.BitSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.log4j.Logger;

public class CoverageData
{
	public static Logger logger = Logger.getLogger(CoverageData.class);
	public final static String TRACER = "set/hyrts/coverage/core/Tracer";
	public static int classId = 0;
	public static ConcurrentMap<String, Integer> classIdMap = new ConcurrentHashMap<String, Integer>();
	public static ConcurrentMap<String, Integer> dotClassIdMap = new ConcurrentHashMap<String, Integer>();
	public static ConcurrentMap<Integer, String> idClassMap = new ConcurrentHashMap<Integer, String>();
	// public static ConcurrentMap<String, String> classNameMap = new
	// ConcurrentHashMap<String, String>();
	public static ConcurrentMap<Integer, ConcurrentMap<Integer, String>> idMethMap = new ConcurrentHashMap<Integer, ConcurrentMap<Integer, String>>();
	public final static int MAX_CLASSNUM = 100000;

	// cache field type info
	public static ConcurrentMap<String, Integer> fieldTypeCache = new ConcurrentHashMap<String, Integer>();
	// cache string type info
	public static ConcurrentMap<String, Integer> stringTypeCache = new ConcurrentHashMap<String, Integer>();
	// cache class type info
	public static ConcurrentMap<Class, Integer> classTypeCache = new ConcurrentHashMap<Class, Integer>();

	// store method coverage: array is approximatedly 0.5 second faster than
	// bitset for joda-time
	public static boolean[][] methCovArray = new boolean[MAX_CLASSNUM][];
	// public static BitSet[] methCovSet = new BitSet[MAX_CLASSNUM];
	// store class coverage
	public static boolean[] classCovArray = new boolean[MAX_CLASSNUM];
	// store line coverage
	public static BitSet[] stmtCovSet = new BitSet[MAX_CLASSNUM];

	// store basic block coverage
	public static BitSet[][] blockCovSet = new BitSet[MAX_CLASSNUM][];

	// store branch coverage
	public static ConcurrentMap<String, Integer> branchCov = new ConcurrentHashMap<String, Integer>();

	public static ConcurrentMap<String, Set<String>> rtType = new ConcurrentHashMap<String, Set<String>>();
	public final static String CLINIT = "<clinit>:()V";

	public static int registerClass(String slashClazz, String dotClazz) {
		int id = nextId();
		classIdMap.put(slashClazz, id);
		dotClassIdMap.put(dotClazz, id);
		idClassMap.put(id, slashClazz);

		// classNameMap.put(slashClazz, dotClazz);
		return id;
	}

	public static int registerMeth(int clazzId, String meth) {
		if (!idMethMap.containsKey(clazzId)) {
			// Map map = new ConcurrentHashMap<Integer, String>();
			ConcurrentMap<Integer, String> map = new ConcurrentHashMap<Integer, String>();
			map.put(0, CLINIT);
			idMethMap.put(clazzId, map);
		}
		if (meth.equals(CLINIT))
			return 0;
		int id = idMethMap.get(clazzId).size();
		idMethMap.get(clazzId).put(id, meth);
		return id;
	}

	private synchronized static int nextId() {
		return classId++;
	}

	public static int decodeClassId(final long value) {
		return (int) (value >> 32);
	}

	public static int decodeMethodId(final long value) {
		return (int) (value & 0xFFFFFFFF);
	}

}
