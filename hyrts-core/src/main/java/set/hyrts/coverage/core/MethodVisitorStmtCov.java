/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.coverage.core;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

class MethodVisitorStmtCov extends MethodVisitor implements Opcodes
{

	int clazzId;
	int methId;

	boolean isVirtual;
	boolean isInit;
	int line;

	public MethodVisitorStmtCov(final MethodVisitor mv, int clazzId, int methId,
			boolean isInit, int access) {
		super(ASM5, mv);
		this.clazzId = clazzId;
		this.methId = methId;
		this.isInit = isInit;
	}

	@Override
	public void visitLabel(Label label) {
		mv.visitLdcInsn(clazzId);
		mv.visitLdcInsn(line);
		mv.visitMethodInsn(INVOKESTATIC, CoverageData.TRACER,
				Tracer.TRACE_STMT_COV, "(II)V", false);
		super.visitLabel(label);
	}

	@Override
	public void visitLineNumber(int line, Label start) {
		this.line = line;
		mv.visitLdcInsn(clazzId);
		mv.visitLdcInsn(line);
		mv.visitMethodInsn(INVOKESTATIC, CoverageData.TRACER,
				Tracer.TRACE_STMT_COV, "(II)V", false);
		super.visitLineNumber(line, start);
	}

}