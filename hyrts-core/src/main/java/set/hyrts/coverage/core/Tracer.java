/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.coverage.core;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

import set.hyrts.utils.Classes;

public class Tracer
{
	public final static String TRACE_CLASS_COV = "traceClassCovInfo";
	public final static String TRACE_METH_COV = "traceMethCovInfo";
	public final static String TRACE_METH_COV_RT = "traceMethCovInfoWithRT";
	public final static String TRACE_CLINIT = "traceMethCovInfoClinit";
	public final static String TRACE_STMT_COV = "traceStmtCovInfo";
	public final static String TRACE_BLOCK_COV = "traceBlockCovInfo";
	public final static String TRACE_BRANCH_COV = "traceBranchCovInfo";

	// trace class coverage
	public static void traceClassCovInfo(int clazzId) {
		CoverageData.classCovArray[clazzId] = true;
	}

	public static void traceClassCovInfo(String clazz) {
		Integer cachedId = CoverageData.stringTypeCache.get(clazz);
		if (cachedId != null) {
			if (cachedId > 0)
				CoverageData.classCovArray[cachedId] = true;
		} else {
			Integer clazzId = CoverageData.classIdMap.get(clazz);
			if (clazzId != null) {
				CoverageData.classCovArray[clazzId] = true;
				CoverageData.stringTypeCache.put(clazz, clazzId);
			} else {
				CoverageData.stringTypeCache.put(clazz, 0);
			}
		}
	}

	public static void traceClassCovInfo(Object clazz) {
		if (clazz != null) {
			traceClassCovInfo(clazz.getClass());
		}
	}

	// trace and cache field access info
	public static void traceClassCovInfo(Object clazz, String field) {
		Integer cachedId = CoverageData.fieldTypeCache.get(field);
		if (cachedId != null) {
			if (cachedId > 0) {
				CoverageData.classCovArray[cachedId] = true;
			}
		} else if (clazz != null) {
			String name = clazz.getClass().getName();
			Integer clazzId = CoverageData.dotClassIdMap.get(name);
			if (clazzId != null) {
				CoverageData.classCovArray[clazzId] = true;
				CoverageData.fieldTypeCache.put(field, clazzId);
			} else {
				CoverageData.fieldTypeCache.put(field, 0);
			}
		} else {
			CoverageData.fieldTypeCache.put(field, 0);
		}
	}

	public static void traceClassCovInfo(Class<?> clazz) {
		Integer cachedId = CoverageData.classTypeCache.get(clazz);
		if (cachedId != null) {
			if (cachedId > 0) {
				CoverageData.classCovArray[cachedId] = true;
			}
		} else {
			String name = clazz.getName();
			Integer clazzId = CoverageData.dotClassIdMap.get(name);
			if (clazzId != null) {
				CoverageData.classCovArray[clazzId] = true;
				CoverageData.classTypeCache.put(clazz, clazzId);
			} else {
				CoverageData.classTypeCache.put(clazz, 0);
			}
		}
	}

	// trace method coverage
	public static void traceMethCovInfo(int clazzId, int methId) {
		CoverageData.methCovArray[clazzId][methId] = true;
	}

	public static void traceMethCovInfoWithRT(int clazzId, int methId,
			String dotClazzName, String methName, String rtype) {
		// System.out.println(">>"+clazzName+"|"+rtype);
		if (rtype.equals(dotClazzName))
			return;
		// bug fix
		Set<String> set = CoverageData.rtType.get(rtype);
		if (set == null) {
			Set<String> newSet = new HashSet<String>();
			newSet.add(methName);
			CoverageData.rtType.put(rtype, newSet);
		} else
			set.add(methName);
	}

	public static void traceMethCovInfoClinit(int clazzId) {
		CoverageData.methCovArray[clazzId][0] = true;
	}

	public static void traceMethCovInfoClinit(String clazz) {
		Integer cachedId = CoverageData.stringTypeCache.get(clazz);
		if (cachedId != null) {
			if (cachedId > 0)
				CoverageData.methCovArray[cachedId][0] = true;
		} else {
			Integer clazzId = CoverageData.classIdMap.get(clazz);
			if (clazzId != null) {
				CoverageData.methCovArray[clazzId][0] = true;
				CoverageData.stringTypeCache.put(clazz, clazzId);
			} else {
				CoverageData.stringTypeCache.put(clazz, 0);
			}
		}
	}

	public static void traceMethCovInfoClinit(Class<?> clazz) {
		Integer cachedId = CoverageData.classTypeCache.get(clazz);
		if (cachedId != null) {
			if (cachedId > 0)
				CoverageData.methCovArray[cachedId][0] = true;
		} else {
			String name = clazz.getName();
			Integer clazzId = CoverageData.dotClassIdMap.get(name);
			if (clazzId != null) {
				CoverageData.methCovArray[clazzId][0] = true;
				CoverageData.classTypeCache.put(clazz, clazzId);
			} else {
				CoverageData.classTypeCache.put(clazz, 0);
			}
		}
	}

	public static void traceMethCovInfoClinit(Object clazz) {
		if (clazz != null) {
			traceMethCovInfoClinit(clazz.getClass());
		}
	}

	// trace statement coverage
	public static void traceStmtCovInfo(int clazzId, int line) {
		CoverageData.stmtCovSet[clazzId].set(line);
	}

	// basic block coverage
	public static void traceBlockCovInfo(int clazzId, int methId, int labelId) {
		CoverageData.blockCovSet[clazzId][methId].set(labelId);
	}

	// trace branch coverage
	public static void traceBranchCovInfo(String branch, String id) {
		String key = id;
		ConcurrentMap<String, Integer> methodMap = CoverageData.branchCov;
		if (branch.length() > 0) {
			key = key + "<" + branch + ">";
			if (branch.equals("true")) {
				String resetKey = id + "<false>";
				methodMap.put(resetKey, methodMap.get(resetKey) - 1);
				if (methodMap.get(resetKey) == 0)
					methodMap.remove(resetKey);
			}
		}
		if (!methodMap.containsKey(key)) {
			methodMap.put(key, 1);
		} else
			methodMap.put(key, methodMap.get(key) + 1);
	}
}
