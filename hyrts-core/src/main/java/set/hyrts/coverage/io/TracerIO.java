/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.coverage.io;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.log4j.Logger;

import set.hyrts.coverage.core.CoverageData;
import set.hyrts.coverage.junit.FTracerJUnitUtils;
import set.hyrts.utils.Classes;
import set.hyrts.utils.Properties;

public class TracerIO
{
	public final static String CLASS = "set/hyrts/coverage/io/TracerIO";
	static Logger logger = Logger.getLogger(TracerIO.class);

	public static String getCovDir() {
		getFTracerDir();
		String dirName = Properties.TRACER_ROOT_DIR + File.separator
				+ Properties.TRACER_COV_TYPE;
		File covDir = new File(dirName);
		covDir.mkdir();
		return dirName;
	}

	public static String getFTracerDir() {
		String rootDirName = Properties.TRACER_ROOT_DIR;
		File rootDir = new File(rootDirName);
		rootDir.mkdir();
		return rootDirName;
	}

	public static void writeClassCov(String testName, String path)
			throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(path));
		writer.write(testName + "\n");
		int classNum = CoverageData.classId;
		// System.out.println("clazzId: "+classNum+"
		// "+CoverageData.idClassMap.size());
		for (int clazzId = 0; clazzId < classNum; clazzId++) {
			if (CoverageData.classCovArray[clazzId]) {
				writer.write(CoverageData.idClassMap.get(clazzId) + "\n");
				CoverageData.classCovArray[clazzId] = false;
			}
		}
		writer.flush();
		writer.close();
	}

	public static void writeMethodCov(String testName, String path)
			throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(path));
		writer.write(testName + "\n");
		int classNum = CoverageData.classId;
		for (int clazzId = 0; clazzId < classNum; clazzId++) {
			boolean[] meths = CoverageData.methCovArray[clazzId];
			if (meths == null)
				continue;
			int methNum = meths.length;
			for (int i = 0; i < methNum; i++) {
				if (meths[i]) {
					// long id = clazzId << 32 + i;
					// writer.write(id + " ");
					writer.write(CoverageData.idClassMap.get(clazzId) + ":"
							+ CoverageData.idMethMap.get(clazzId).get(i)
							+ "\n");
					meths[i] = false;
				}
			}
		}
		writer.flush();
		writer.close();
	}

	public static void writeMethodCovWithRT(String testName, String path)
			throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(path));
		writer.write(testName + "\n");
		int classNum = CoverageData.classId;
		for (int clazzId = 0; clazzId < classNum; clazzId++) {
			boolean[] meths = CoverageData.methCovArray[clazzId];
			if (meths == null)
				continue;
			int methNum = meths.length;
			for (int i = 0; i < methNum; i++) {
				if (meths[i]) {
					// long id = clazzId << 32 + i;
					// writer.write(id + " ");
					writer.write(CoverageData.idClassMap.get(clazzId) + ":"
							+ CoverageData.idMethMap.get(clazzId).get(i)
							+ "\n");
					meths[i] = false;
				}
			}
		}
		for (String rType : CoverageData.rtType.keySet()) {
			String slashClazzName = Classes.toSlashClassName(rType);
			for (String meth : CoverageData.rtType.get(rType)) {
				writer.write(slashClazzName + ":" + meth + "\n");
			}
			CoverageData.rtType.get(rType).clear();
		}
		writer.flush();
		writer.close();
	}

	public static void writeStmtCov(String testName, String path)
			throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(path));
		writer.write(testName + "\n");
		int classNum = CoverageData.classId;
		for (int clazzId = 0; clazzId < classNum; clazzId++) {
			BitSet stmts = CoverageData.stmtCovSet[clazzId];
			if (stmts == null)
				continue;
			int stmtNum = stmts.size();
			for (int i = 0; i < stmtNum; i++) {
				if (stmts.get(i)) {
					writer.write(CoverageData.idClassMap.get(clazzId) + ":" + i
							+ "\n");
					stmts.clear(i);
				}
			}
		}
		writer.flush();
		writer.close();
	}

	public static void writeBranchCov(String testName, String path)
			throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(path));
		writer.write(testName + "\n");
		for (String branch : CoverageData.branchCov.keySet()) {
			writer.write(branch + "\n");
		}
		CoverageData.branchCov.clear();
		writer.flush();
		writer.close();
	}

	public static void writeBlockCovWithRT(String testName, String path)
			throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(path));
		writer.write(testName + "\n");
		int classNum = CoverageData.classId;
		// write block cov info
		for (int clazzId = 0; clazzId < classNum; clazzId++) {
			BitSet[] meths = CoverageData.blockCovSet[clazzId];
			if (meths == null)
				continue;
			int methNum = meths.length;
			for (int i = 0; i < methNum; i++) {
				if (!CoverageData.methCovArray[clazzId][i]) {
					continue;
				}
				writer.write(CoverageData.idClassMap.get(clazzId) + ":"
						+ CoverageData.idMethMap.get(clazzId).get(i));
				BitSet labels = meths[i];
				for (int j = 0; j < labels.size(); j++) {
					if (labels.get(j)) {
						writer.write(" " + j);
						labels.clear(j);
					}
				}
				writer.write("\n");
			}
		}
		// write method coverage, not necessary, since subsumed by block cov
		for (int clazzId = 0; clazzId < classNum; clazzId++) {
			boolean[] meths = CoverageData.methCovArray[clazzId];
			if (meths == null)
				continue;
			int methNum = meths.length;
			for (int i = 0; i < methNum; i++) {
				if (meths[i]) {
					// not necessary since subsumed by block cov
					// writer.write(CoverageData.idClassMap.get(clazzId) + ":"
					// + CoverageData.idMethMap.get(clazzId).get(i)
					// + "\n");
					meths[i] = false;
				}
			}
		}
		// write run-time info
		for (String rType : CoverageData.rtType.keySet()) {
			String slashClazzName = Classes.toSlashClassName(rType);
			for (String meth : CoverageData.rtType.get(rType)) {
				writer.write(slashClazzName + ":" + meth + "\n");
			}
			CoverageData.rtType.get(rType).clear();
		}
		writer.flush();
		writer.close();
	}

	public static Map<String, Set<String>> loadCovFromDirectory(String dirPath)
			throws IOException {
		File dir = new File(
				dirPath + File.separator + Properties.TRACER_ROOT_DIR
						+ File.separator + Properties.TRACER_COV_TYPE);
		logger.debug("Loading from " + dir);
		if (!dir.exists()) {
			logger.info("Directory does not exist: " + dir);
			return null;
		}
		File[] tests = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(Properties.GZ_EXT);
			}
		});
		Map<String, Set<String>> result = new HashMap<String, Set<String>>();
		for (File f : tests) {
			loadCoverage(f, result);
		}
		return result;
	}

	public static void loadCoverage(File file, Map<String, Set<String>> result)
			throws IOException {
		Set<String> coveredElements = new HashSet<String>();
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String testName = reader.readLine();
		String line = reader.readLine();
		while (line != null) {
			coveredElements.add(line);
			line = reader.readLine();
		}
		reader.close();
		result.put(testName, coveredElements);
	}

	public static void cleanObsoleteTestCov() {
		System.out.println(Properties.HYRTS_TAG + "Run "
				+ FTracerJUnitUtils.runnedTests.size() + "/"
				+ FTracerJUnitUtils.allTests.size() + " tests for "
				+ System.getProperty("user.dir"));
		File dir = new File(Properties.TRACER_ROOT_DIR + File.separator
				+ Properties.TRACER_COV_TYPE);
		if (!dir.exists())
			return;
		File[] tests = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(Properties.GZ_EXT);
			}
		});
		int testCovNum = tests.length;
		for (File test : tests) {
			String testName = test.getName().replace(Properties.GZ_EXT, "");
			if (!FTracerJUnitUtils.allTests.contains(testName)) {
				test.delete();
				testCovNum--;
			}
		}
		// consistency check for AEC mode
		if (!Properties.EXECUTION_ONLY) {
			if (FTracerJUnitUtils.allTests.size() != testCovNum) {
				System.out.println(Properties.HYRTS_TAG
						+ "ERROR: test coverage set (" + testCovNum
						+ ") inconsistent with mvn test set ("
						+ FTracerJUnitUtils.allTests.size() + ")");
			} else {
				System.out.println(Properties.HYRTS_TAG
						+ "Test coverage set consistent with mvn test set: total "
						+ testCovNum + ", reran "
						+ FTracerJUnitUtils.runnedTests.size());
			}
		}

	}

	public static String getTestCovFilePath(String dirPath, String test) {
		File rootDir = new File(
				dirPath + File.separator + Properties.TRACER_ROOT_DIR
						+ File.separator + Properties.TRACER_COV_TYPE);
		rootDir.mkdirs();
		return rootDir.getAbsolutePath() + File.separator
				+ FTracerJUnitUtils.getFileName(test) + Properties.GZ_EXT;
	}

	public static String getHyRTSCovDir(String dirPath) {
		File rootDir = new File(
				dirPath + File.separator + Properties.TRACER_ROOT_DIR
						+ File.separator + Properties.TRACER_COV_TYPE);
		rootDir.mkdirs();
		return rootDir.getAbsolutePath();
	}
}
