/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.coverage.io;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TransformPomJUnitVersion
{
	public static void main(String[] args) throws Exception {
		// String inPath =
		// "/Users/lingmingzhang/Research/data/github-source/joda-time/pom.xml";
		// String outPath =
		// "/Users/lingmingzhang/Research/data/github-source/joda-time/pom-ftracer.xml";
		// String prefix="org.joda.time";
		String inPath = args[0];// "pom.xml";
		String outPath = args[1];// "pom-ftracer.xml";
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(inPath);

		// Get the root element
		XPath xpath = XPathFactory.newInstance().newXPath();

		String[] pluginPaths = { "//project/build/pluginManagement/plugins",
				"//project/build/plugins" };
		for (String pluginPath : pluginPaths) {
			XPathExpression expr1 = xpath.compile(pluginPath);
			NodeList nlist = (NodeList) expr1.evaluate(doc,
					XPathConstants.NODESET);
			Node plugins = nlist.item(0);
			if (plugins == null)
				continue;

			// loop the plugins child node
			NodeList list = plugins.getChildNodes();
			for (int i = 0; i < list.getLength(); i++) {
				Node node = list.item(i);
				if (!"plugin".equals(node.getNodeName()))
					continue;
				for (int j = 0; j < node.getChildNodes().getLength(); j++) {
					if ("maven-surefire-plugin".equals(
							node.getChildNodes().item(j).getTextContent())) {
						boolean containsVersion = false;
						String surefireVer = "2.19.1";
						for (int k = 0; k < node.getChildNodes()
								.getLength(); k++) {
							Node n = node.getChildNodes().item(k);
							// remove argLine and fork settings to run
							// hyrts
							if (n.getNodeName().equals("configuration")) {
								Set<Node> toRemove = new HashSet<Node>();
								for (int l = 0; l < n.getChildNodes()
										.getLength(); l++) {
									Node line = n.getChildNodes().item(l);
									if (line.getNodeName().equals("argLine")
											|| line.getNodeName().toLowerCase()
													.contains("fork")
											|| line.getNodeName().equals("skip")
											|| line.getNodeName()
													.equals("parallel")
											|| line.getNodeName()
													.equals("threadCount")
											|| line.getNodeName().equals(
													"perCoreThreadCount")) {
										toRemove.add(line);
									}
								}
								for (Node del : toRemove) {
									n.removeChild(del);
								}
							} else if (n.getNodeName().equals("version")) {
								n.setTextContent(surefireVer);
								containsVersion = true;
							}
						}
						if (!containsVersion) {
							Node v = doc.createElement("version");
							v.setTextContent(surefireVer);
							node.appendChild(v);
						}
						break;
					}
				}
			}
		}
		XPathExpression project_expr = xpath.compile("//project");
		NodeList proj_list = (NodeList) project_expr.evaluate(doc,
				XPathConstants.NODESET);
		Node proj = proj_list.item(0);
		XPathExpression expr2 = xpath.compile("//project/dependencies");
		NodeList nlist2 = (NodeList) expr2.evaluate(doc,
				XPathConstants.NODESET);
		Node dependencies = nlist2.item(0);
		if (dependencies == null) {
			dependencies = doc.createElement("dependencies");
			proj.appendChild(dependencies);
		}

		// add asm dependency to avoid version inconsistencies
		Element dependency = doc.createElement("dependency");
		Element dependencyGroup = doc.createElement("groupId");
		dependencyGroup.appendChild(doc.createTextNode("org.ow2.asm"));
		Element dependencyArtifact = doc.createElement("artifactId");
		dependencyArtifact.appendChild(doc.createTextNode("asm"));
		Element dependencyVersion = doc.createElement("version");
		dependencyVersion.appendChild(doc.createTextNode("5.0.3"));
		dependency.appendChild(dependencyGroup);
		dependency.appendChild(dependencyArtifact);
		dependency.appendChild(dependencyVersion);
		dependencies.insertBefore(dependency, dependencies.getFirstChild());

		// change junit version
		for (int i = 0; i < dependencies.getChildNodes().getLength(); i++) {
			Node node = dependencies.getChildNodes().item(i);
			if (!"dependency".equals(node.getNodeName()))
				continue;
			boolean isJunit = false;
			for (int j = 0; j < node.getChildNodes().getLength(); j++) {
				if (node.getChildNodes().item(j).getNodeName().equals("groupId")
						&& node.getChildNodes().item(j).getTextContent()
								.equals("junit")) {
					isJunit = true;
					break;
				}
			}
			if (isJunit) {
				for (int j = 0; j < node.getChildNodes().getLength(); j++) {
					if (node.getChildNodes().item(j).getNodeName()
							.equals("version"))
						node.getChildNodes().item(j).setTextContent("4.12");
				}
			}
		}

		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory
				.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(outPath));
		transformer.transform(source, result);
	}

}
