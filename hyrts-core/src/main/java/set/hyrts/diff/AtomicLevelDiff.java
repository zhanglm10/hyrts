/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.diff;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import set.hyrts.utils.Properties;

/**
 * Compute the atomic changes inside changed files
 * 
 * @author lingmingzhang
 *
 */
public class AtomicLevelDiff
{

	// diff two class files in detail
	public static void diff(String clazz) throws IOException {

		if (!VersionDiff.oldClassHeaders.get(clazz)
				.equals(VersionDiff.newClassHeaders.get(clazz))) {
			VersionDiff.classHeaderChanges.add(clazz);
			return;
		}
		Map<String, String> oldMethodMap = VersionDiff.oldClassMeths.get(clazz);
		Map<String, String> newMethodMap = VersionDiff.newClassMeths.get(clazz);
		Set<String> DI_toAdd = new HashSet<String>();
		Set<String> DSI_toAdd = new HashSet<String>();
		Set<String> DIM_toAdd = new HashSet<String>();
		Set<String> DSM_toAdd = new HashSet<String>();
		Set<String> AI_toAdd = new HashSet<String>();
		Set<String> ASI_toAdd = new HashSet<String>();
		Set<String> AIM_toAdd = new HashSet<String>();
		Set<String> ASM_toAdd = new HashSet<String>();
		Set<String> CI_toAdd = new HashSet<String>();
		Set<String> CSI_toAdd = new HashSet<String>();
		Set<String> CIM_toAdd = new HashSet<String>();
		Set<String> CSM_toAdd = new HashSet<String>();
		Set<String> LC_toAdd = new HashSet<String>();

		// obtain changed and deleted methods
		for (String method : oldMethodMap.keySet()) {
			int staticTag = oldMethodMap.get(method).charAt(0) - '0';
			if (!newMethodMap.containsKey(method)) {
				if (method.startsWith("<init>")) {
					if (VersionDiff.transDI) {
						VersionDiff.transformedClassChanges.add(clazz);
						return;
					}
					DI_toAdd.add(clazz + ":" + method);
				} else if (method.startsWith("<clinit>")) {
					if (VersionDiff.transDSI) {
						VersionDiff.transformedClassChanges.add(clazz);
						return;
					}
					DSI_toAdd.add(clazz + ":" + method);
				} else if (staticTag == 1) {
					if (VersionDiff.transDSM) {
						VersionDiff.transformedClassChanges.add(clazz);
						return;
					}
					DSM_toAdd.add(clazz + ":" + method);
				} else {
					if (VersionDiff.transDIM) {
						VersionDiff.transformedClassChanges.add(clazz);
						return;
					}
					DIM_toAdd.add(clazz + ":" + method);
					// obtain the method lookup changes
					Set<String> impactedClasses = ClassInheritanceGraph
							.findLookUpMethods(clazz, method, VersionDiff.oldClassMeths);
					for (String impactedClass : impactedClasses) {
						LC_toAdd.add(impactedClass + ":" + method);
					}
				}
			} else {
				if (!oldMethodMap.get(method)
						.equals(newMethodMap.get(method))) {
					if (method.startsWith("<init>")) {
						if (VersionDiff.transCI) {
							VersionDiff.transformedClassChanges.add(clazz);
							return;
						}
						CI_toAdd.add(clazz + ":" + method);
					} else if (method.startsWith("<clinit>")) {
						if (VersionDiff.transCSI) {
							VersionDiff.transformedClassChanges.add(clazz);
							return;
						}
						CSI_toAdd.add(clazz + ":" + method);
					} else if (staticTag == 1) {
						if (VersionDiff.transCSM) {
							VersionDiff.transformedClassChanges.add(clazz);
							return;
						}
						CSM_toAdd.add(clazz + ":" + method);
					} else {
						if (VersionDiff.transCIM) {
							VersionDiff.transformedClassChanges.add(clazz);
							return;
						}
						CIM_toAdd.add(clazz + ":" + method);
					}
					// deal with detailed basic block changes
					if (Properties.TRACER_COV_TYPE.endsWith(Properties.BLK_COV)) {
						String newPath = ClassContentParser.classResource
								.get(clazz);
						String oldPath = VersionDiff.OLDDIR
								+ ClassContentParser.classResource.get(clazz)
										.substring(VersionDiff.OLDDIR.length());
						VersionDiff.BLKs.addAll((new CFGDiff()).diff(oldPath,
								newPath, method));
					}
				}
			}
			newMethodMap.remove(method);
		}

		for (String method : newMethodMap.keySet()) {
			int staticTag = newMethodMap.get(method).charAt(0) - '0';
			if (method.startsWith("<init>")) {
				if (VersionDiff.transAI) {
					VersionDiff.transformedClassChanges.add(clazz);
					return;
				}
				AI_toAdd.add(clazz + ":" + method);
			} else if (method.startsWith("<clinit>")) {
				if (VersionDiff.transASI) {
					VersionDiff.transformedClassChanges.add(clazz);
					return;
				}
				ASI_toAdd.add(clazz + ":" + method);
			} else if (staticTag == 1) {
				if (VersionDiff.transASM) {
					VersionDiff.transformedClassChanges.add(clazz);
					return;
				}
				ASM_toAdd.add(clazz + ":" + method);
			} else {
				if (VersionDiff.transAIM) {
					VersionDiff.transformedClassChanges.add(clazz);
					return;
				}
				AIM_toAdd.add(clazz + ":" + method);
				// obtain the method lookup changes
				Set<String> impactedClasses = ClassInheritanceGraph
						.findLookUpMethods(clazz, method, VersionDiff.oldClassMeths);
				for (String impactedClass : impactedClasses) {
					LC_toAdd.add(impactedClass + ":" + method);
				}
			}
		}
		VersionDiff.AIs.addAll(AI_toAdd);
		VersionDiff.ASIs.addAll(ASI_toAdd);
		VersionDiff.AIMs.addAll(AIM_toAdd);
		VersionDiff.ASMs.addAll(ASM_toAdd);
		VersionDiff.DIs.addAll(DI_toAdd);
		VersionDiff.DSIs.addAll(DSI_toAdd);
		VersionDiff.DIMs.addAll(DIM_toAdd);
		VersionDiff.DSMs.addAll(DSM_toAdd);
		VersionDiff.CIs.addAll(CI_toAdd);
		VersionDiff.CSIs.addAll(CSI_toAdd);
		VersionDiff.CIMs.addAll(CIM_toAdd);
		VersionDiff.CSMs.addAll(CSM_toAdd);
		VersionDiff.LCs.addAll(LC_toAdd);
	}

}
