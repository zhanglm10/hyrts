/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.diff;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.log4j.Logger;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

import set.hyrts.utils.Properties;

public class ClassContentParser
{
	static Map<String, String> classResource=new HashMap<String, String>();
	private static Logger logger = Logger.getLogger(ClassContentParser.class);

	public static void parseAndSerializeNewContents(
			Set<ClassFileHandler> cFiles, String dir) throws IOException {
		classResource.clear();
		String ftracerDir = getFTracerDir(dir);
		String fileName = Properties.FILE_CHECKSUM + Properties.GZ_EXT;
		ObjectOutputStream oos = new ObjectOutputStream(new GZIPOutputStream(
				new FileOutputStream(ftracerDir + File.separator + fileName)));
		oos.writeInt(cFiles.size());

		for (ClassFileHandler cFile : cFiles) {
			InputStream is = cFile.getInputStream();
			ClassNode node = new ClassNode(Opcodes.ASM5);
			ClassReader cr = new ClassReader(is);
			cr.accept(node, ClassReader.SKIP_DEBUG);

			String className = node.name;
			String superClass = node.superName == null ? "" : node.superName;
			String checkSum = CheckSum.compute(ContentPrinter.print(node));
			oos.writeUTF(className);
			oos.writeUTF(checkSum);

			if (Properties.TRACER_COV_TYPE.endsWith(Properties.CLASS_COV)) {
				VersionDiff.newClasses.put(className, checkSum);
				continue;
			}

			String headCheckSum = "";
			Map<String, String> methodMap = new HashMap<String, String>();
			if (checkSum.equals(VersionDiff.oldClasses.get(className))) {
				// read header checksum from prior version
				headCheckSum = VersionDiff.oldClassHeaders.get(className);
				// read method checksum from prior version
				methodMap = VersionDiff.oldClassMeths.get(className);
				// remove the matched classes to save time
				VersionDiff.oldClasses.remove(className);
				VersionDiff.oldClassHeaders.remove(className);
			} else {
				// get class header checksum
				headCheckSum = CheckSum
						.compute(ContentPrinter.printClassHeader(node));
				// get the method list and their checksum info
				List<MethodNode> methods = node.methods;
				for (MethodNode method : methods) {
					String methodId = methodName(method);
					String methodContent = ContentPrinter.print(method);
					// record whether it's static or virtual method
					int tag = ((method.access & Opcodes.ACC_STATIC) == 0)?0:1;
					methodMap.put(methodId, tag+CheckSum.compute(methodContent));
				}
				classResource.put(className, cFile.filePath);
				VersionDiff.newClasses.put(className, checkSum);
				VersionDiff.newClassMeths.put(className, methodMap);
				VersionDiff.newClassHeaders.put(className, headCheckSum);
			}
			oos.writeUTF(superClass);
			oos.writeUTF(headCheckSum);
			oos.writeInt(methodMap.size());
			for (String meth : methodMap.keySet()) {
				oos.writeUTF(meth);
				oos.writeUTF(methodMap.get(meth));
			}
			is.close();
		}
		oos.flush();
		oos.close();
	}

	public static void deserializeOldContents(String dir) throws IOException {
		String ftracerDir = getFTracerDir(dir);
		String fileName = Properties.FILE_CHECKSUM + Properties.GZ_EXT;
		File f = new File(ftracerDir + File.separator + fileName);
		if (!f.exists())
			return;
		ObjectInputStream ois = new ObjectInputStream(
				new GZIPInputStream(new FileInputStream(f)));
		int classNum = ois.readInt();
		for (int i = 0; i < classNum; i++) {
			String clazz = ois.readUTF();
			String checkSum = ois.readUTF();
			VersionDiff.oldClasses.put(clazz, checkSum);
			if (Properties.TRACER_COV_TYPE.endsWith(Properties.CLASS_COV))
				continue;
			String superClass = ois.readUTF();
			String headCheckSum = ois.readUTF();
			Map<String, String> methCheckSum = new HashMap<String, String>();
			int methNum = ois.readInt();
			for (int j = 0; j < methNum; j++) {
				String meth = ois.readUTF();
				String mCheckSum = ois.readUTF();
				methCheckSum.put(meth, mCheckSum);
			}
			VersionDiff.oldClasses.put(clazz, checkSum);
			VersionDiff.oldClassMeths.put(clazz, methCheckSum);
			VersionDiff.oldClassHeaders.put(clazz, headCheckSum);
			// if both DIM and AIM changes are transformed into class changes,
			// we
			// don't need to trace method overriding changes
			if (VersionDiff.transDIM && VersionDiff.transAIM) {
				continue;
			}
			if (!superClass.equals("java/lang/Object")) {
				if (!ClassInheritanceGraph.inheritanceMap
						.containsKey(superClass))
					ClassInheritanceGraph.inheritanceMap.put(superClass,
							new HashSet<String>());
				ClassInheritanceGraph.inheritanceMap.get(superClass).add(clazz);
			}

		}
		ois.close();
	}

	public static String methodName(MethodNode method) {
		return method.name + ":" + method.desc;
	}

	public static String fieldName(FieldNode field) {
		return field.name + ":" + field.desc;
	}

	public static String getFTracerDir(String dir) {
		String rootDirName;
		if (dir.length() > 0)
			rootDirName = dir + File.separator + Properties.TRACER_ROOT_DIR;
		else
			rootDirName = Properties.TRACER_ROOT_DIR;
		File rootDir = new File(rootDirName);
		rootDir.mkdir();
		return rootDirName;
	}
}
