/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.diff;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.jar.JarFile;

public class ClassFileHandler
{
	public boolean isJar = false;
	public String filePath;
	public String className;
	public JarFile jarFile;
	//public String md5;

	public ClassFileHandler(String className, String filePath)
			throws IOException {
		isJar = false;
		this.className = className;
		this.filePath = filePath;

//		InputStream classFileInputStream = getInputStream();
//		md5 = CheckSum.compute(classFileInputStream);
//		classFileInputStream.close();
	}

	public ClassFileHandler(String className, JarFile jar) throws IOException {
		isJar = true;
		this.className = className;
		this.jarFile = jar;

//		InputStream classFileInputStream = getInputStream();
//		md5 = CheckSum.compute(classFileInputStream);
//		classFileInputStream.close();
	}
 
	public InputStream getInputStream() throws IOException {
		InputStream is = null;
		if (isJar)
			is = jarFile.getInputStream(jarFile.getEntry(className + ".class"));
		else
			is = new FileInputStream(filePath);
		return is;
	}

}
