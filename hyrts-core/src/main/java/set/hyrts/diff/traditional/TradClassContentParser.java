/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.diff.traditional;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.log4j.Logger;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

import set.hyrts.diff.*;
import set.hyrts.utils.Properties;

public class TradClassContentParser
{
	static Map<String, String> classResource = new HashMap<String, String>();
	private static Logger logger = Logger
			.getLogger(TradClassContentParser.class);

	public static void parseAndSerializeNewContents(
			Set<ClassFileHandler> cFiles, String dir) throws IOException {
		classResource.clear();
		String ftracerDir = getFTracerDir(dir);
		String fileName = Properties.FILE_CHECKSUM + Properties.GZ_EXT;
		ObjectOutputStream oos = new ObjectOutputStream(new GZIPOutputStream(
				new FileOutputStream(ftracerDir + File.separator + fileName)));
		oos.writeInt(cFiles.size());

		for (ClassFileHandler cFile : cFiles) {
			InputStream is = cFile.getInputStream();
			ClassNode node = new ClassNode(Opcodes.ASM5);
			ClassReader cr = new ClassReader(is);
			cr.accept(node, ClassReader.SKIP_DEBUG);

			String className = node.name;
			String superClass = node.superName == null ? "" : node.superName;
			oos.writeUTF(className);
			
			// get class header checksum
			String headContent = controlledHash(ContentPrinter.printClassHeader(node));
			Map<String, String> methodMap = new HashMap<String, String>();
			// get the method list and their checksum info
			List<MethodNode> methods = node.methods;
			for (MethodNode method : methods) {
				String methodId = methodName(method);
				String methodContent = controlledHash(ContentPrinter.print(method));
				// record whether it's static or virtual method
				int tag = ((method.access & Opcodes.ACC_STATIC) == 0) ? 0 : 1;
				methodMap.put(methodId, tag + (methodContent));
			}
			classResource.put(className, cFile.filePath);
			TradMethVersionDiff.newClassMeths.put(className, methodMap);
			TradMethVersionDiff.newClassHeaders.put(className, headContent);
			oos.writeUTF(superClass);
			encodeWriteLargeString(oos,headContent);
			oos.writeInt(methodMap.size());
			for (String meth : methodMap.keySet()) {
				oos.writeUTF(meth);
				//oos.writeUTF(methodMap.get(meth));
				encodeWriteLargeString(oos,methodMap.get(meth));
			}
			is.close();
		}
		oos.flush();
		oos.close();
	}

	public static void deserializeOldContents(String dir) throws IOException {
		String ftracerDir = getFTracerDir(dir);
		String fileName = Properties.FILE_CHECKSUM + Properties.GZ_EXT;
		File f = new File(ftracerDir + File.separator + fileName);
		if (!f.exists())
			return;
		ObjectInputStream ois = new ObjectInputStream(
				new GZIPInputStream(new FileInputStream(f)));
		int classNum = ois.readInt();
		for (int i = 0; i < classNum; i++) {
			String clazz = ois.readUTF();
			String superClass = ois.readUTF();
			String headContent = decodeReadLargeString(ois);
			Map<String, String> methContent = new HashMap<String, String>();
			int methNum = ois.readInt();
			for (int j = 0; j < methNum; j++) {
				String meth = ois.readUTF();
				String mContent = decodeReadLargeString(ois);
				methContent.put(meth, mContent);
			}
			TradMethVersionDiff.oldClassMeths.put(clazz, methContent);
			TradMethVersionDiff.oldClassHeaders.put(clazz, headContent);

			if (!superClass.equals("java/lang/Object")) {
				if (!ClassInheritanceGraph.inheritanceMap
						.containsKey(superClass))
					ClassInheritanceGraph.inheritanceMap.put(superClass,
							new HashSet<String>());
				ClassInheritanceGraph.inheritanceMap.get(superClass).add(clazz);
			}

		}
		ois.close();
	}
	
	public static String controlledHash(String s) throws IOException{
		return s;
		//return CheckSum.compute(s); 
	}
	
	public static void encodeWriteLargeString(ObjectOutputStream out, String str) throws IOException{
		// Write data
		byte[] data=str.getBytes("UTF-8");
		out.writeInt(data.length);
		out.write(data);
	}
	public static String decodeReadLargeString(ObjectInputStream in) throws IOException{
		// Read data
		int length=in.readInt();
		byte[] data=new byte[length];
		in.readFully(data);
		String str=new String(data,"UTF-8");
		return str;
	}

	public static String methodName(MethodNode method) {
		return method.name + ":" + method.desc;
	}

	public static String fieldName(FieldNode field) {
		return field.name + ":" + field.desc;
	}

	public static String getFTracerDir(String dir) {
		String rootDirName;
		if (dir.length() > 0)
			rootDirName = dir + File.separator + Properties.TRACER_ROOT_DIR;
		else
			rootDirName = Properties.TRACER_ROOT_DIR;
		File rootDir = new File(rootDirName);
		rootDir.mkdir();
		return rootDirName;
	}
}
