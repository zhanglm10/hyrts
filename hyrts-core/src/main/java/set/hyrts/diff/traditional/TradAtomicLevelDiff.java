/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.diff.traditional;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import set.hyrts.diff.*;
import set.hyrts.utils.Properties;

/**
 * Compute the atomic changes inside changed files
 * 
 * @author lingmingzhang
 *
 */
public class TradAtomicLevelDiff
{
	/**
	 * Transfer all class changes into method level changes
	 * 
	 * @param clazz
	 */
	public static void getAllDels(String clazz) {
		Map<String, String> oldMethodMap = TradMethVersionDiff.oldClassMeths
				.get(clazz);
		for (String method : oldMethodMap.keySet()) {
			int staticTag = oldMethodMap.get(method).charAt(0) - '0';
			if (method.startsWith("<init>")) {
				TradMethVersionDiff.DIs.add(clazz + ":" + method);
			} else if (method.startsWith("<clinit>")) {
				TradMethVersionDiff.DSIs.add(clazz + ":" + method);
			} else if (staticTag == 1) {
				TradMethVersionDiff.DSMs.add(clazz + ":" + method);
			} else {
				TradMethVersionDiff.DIMs.add(clazz + ":" + method);
				// obtain the method lookup changes
				Set<String> impactedClasses = ClassInheritanceGraph
						.findLookUpMethods(clazz, method, TradMethVersionDiff.oldClassMeths);
				for (String impactedClass : impactedClasses) {
					TradMethVersionDiff.LCs.add(impactedClass + ":" + method);
				}
			}
		}
	}

	public static void getAllAdds(String clazz) {
		Map<String, String> newMethodMap = TradMethVersionDiff.newClassMeths
				.get(clazz);
		for (String method : newMethodMap.keySet()) {
			int staticTag = newMethodMap.get(method).charAt(0) - '0';
			if (method.startsWith("<init>")) {
				TradMethVersionDiff.AIs.add(clazz + ":" + method);
			} else if (method.startsWith("<clinit>")) {
				TradMethVersionDiff.ASIs.add(clazz + ":" + method);
			} else if (staticTag == 1) {
				TradMethVersionDiff.ASMs.add(clazz + ":" + method);
			} else {
				TradMethVersionDiff.AIMs.add(clazz + ":" + method);
				// obtain the method lookup changes
				Set<String> impactedClasses = ClassInheritanceGraph
						.findLookUpMethods(clazz, method,
								TradMethVersionDiff.oldClassMeths);
				for (String impactedClass : impactedClasses) {
					TradMethVersionDiff.LCs.add(impactedClass + ":" + method);
				}
			}
		}
	}

	// diff two class files in detail
	public static boolean diff(String clazz) throws IOException {
		boolean changed = false;
		if (!TradMethVersionDiff.oldClassHeaders.get(clazz)
				.equals(TradMethVersionDiff.newClassHeaders.get(clazz))) {
			getAllDels(clazz);
			changed = true;
			return changed;
		}
		Map<String, String> oldMethodMap = TradMethVersionDiff.oldClassMeths
				.get(clazz);
		Map<String, String> newMethodMap = TradMethVersionDiff.newClassMeths
				.get(clazz);
		Set<String> DI_toAdd = new HashSet<String>();
		Set<String> DSI_toAdd = new HashSet<String>();
		Set<String> DIM_toAdd = new HashSet<String>();
		Set<String> DSM_toAdd = new HashSet<String>();
		Set<String> AI_toAdd = new HashSet<String>();
		Set<String> ASI_toAdd = new HashSet<String>();
		Set<String> AIM_toAdd = new HashSet<String>();
		Set<String> ASM_toAdd = new HashSet<String>();
		Set<String> CI_toAdd = new HashSet<String>();
		Set<String> CSI_toAdd = new HashSet<String>();
		Set<String> CIM_toAdd = new HashSet<String>();
		Set<String> CSM_toAdd = new HashSet<String>();
		Set<String> LC_toAdd = new HashSet<String>();

		// obtain changed and deleted methods
		for (String method : oldMethodMap.keySet()) {
			int staticTag = oldMethodMap.get(method).charAt(0) - '0';
			if (!newMethodMap.containsKey(method)) {
				if (method.startsWith("<init>")) {
					DI_toAdd.add(clazz + ":" + method);
				} else if (method.startsWith("<clinit>")) {
					DSI_toAdd.add(clazz + ":" + method);
				} else if (staticTag == 1) {
					DSM_toAdd.add(clazz + ":" + method);
				} else {
					DIM_toAdd.add(clazz + ":" + method);
					// obtain the method lookup changes
					Set<String> impactedClasses = ClassInheritanceGraph
							.findLookUpMethods(clazz, method,
									TradMethVersionDiff.oldClassMeths);
					for (String impactedClass : impactedClasses) {
						LC_toAdd.add(impactedClass + ":" + method);
					}
				}
				changed = true;
			} else {
				if (!oldMethodMap.get(method)
						.equals(newMethodMap.get(method))) {
					if (method.startsWith("<init>")) {
						CI_toAdd.add(clazz + ":" + method);
					} else if (method.startsWith("<clinit>")) {
						CSI_toAdd.add(clazz + ":" + method);
					} else if (staticTag == 1) {
						CSM_toAdd.add(clazz + ":" + method);
					} else {
						CIM_toAdd.add(clazz + ":" + method);
					}
					changed = true;
				}
			}
			newMethodMap.remove(method);
		}

		for (String method : newMethodMap.keySet()) {
			int staticTag = newMethodMap.get(method).charAt(0) - '0';
			if (method.startsWith("<init>")) {
				AI_toAdd.add(clazz + ":" + method);
			} else if (method.startsWith("<clinit>")) {
				ASI_toAdd.add(clazz + ":" + method);
			} else if (staticTag == 1) {
				ASM_toAdd.add(clazz + ":" + method);
			} else {
				AIM_toAdd.add(clazz + ":" + method);
				// obtain the method lookup changes
				Set<String> impactedClasses = ClassInheritanceGraph
						.findLookUpMethods(clazz, method,
								TradMethVersionDiff.oldClassMeths);
				for (String impactedClass : impactedClasses) {
					LC_toAdd.add(impactedClass + ":" + method);
				}
			}
			changed = true;
		}
		TradMethVersionDiff.AIs.addAll(AI_toAdd);
		TradMethVersionDiff.ASIs.addAll(ASI_toAdd);
		TradMethVersionDiff.AIMs.addAll(AIM_toAdd);
		TradMethVersionDiff.ASMs.addAll(ASM_toAdd);
		TradMethVersionDiff.DIs.addAll(DI_toAdd);
		TradMethVersionDiff.DSIs.addAll(DSI_toAdd);
		TradMethVersionDiff.DIMs.addAll(DIM_toAdd);
		TradMethVersionDiff.DSMs.addAll(DSM_toAdd);
		TradMethVersionDiff.CIs.addAll(CI_toAdd);
		TradMethVersionDiff.CSIs.addAll(CSI_toAdd);
		TradMethVersionDiff.CIMs.addAll(CIM_toAdd);
		TradMethVersionDiff.CSMs.addAll(CSM_toAdd);
		TradMethVersionDiff.LCs.addAll(LC_toAdd);
		return changed;
	}

}
