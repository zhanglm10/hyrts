/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.diff;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

/**
 * Compute the class inheritance graph for the entire code
 * 
 * @author lingmingzhang
 *
 */
public class ClassInheritanceGraph
{
	public static Map<String, Set<String>> inheritanceMap = new HashMap<String, Set<String>>();
	// static Map<String, Set<String>> classMethodMap = new HashMap<String,
	// Set<String>>();
	// static Map<String, Set<String>> classFieldMap = new HashMap<String,
	// Set<String>>();

	public static void clear() {
		// classFieldMap.clear();
		// classMethodMap.clear();
		inheritanceMap.clear();
	}

	// find all the classes that can be impacted by the method addition
	public static Set<String> findLookUpMethods(String clazz, String method, Map<String, Map<String, String>> oldClassMeths) {
		Set<String> res = new HashSet<String>();
		List<String> workList = new ArrayList<String>();
		workList.add(clazz);
		res.add(clazz);
		while (!workList.isEmpty()) {
			String curClazz = workList.remove(0);
			if (inheritanceMap.containsKey(curClazz)) {
				for (String subClass : inheritanceMap.get(curClazz)) {
					if (!oldClassMeths.get(subClass)
							.containsKey(method)) {
						res.add(subClass);
						workList.add(subClass);
					}
				}
			}
		}
		return res;
	}

}
