/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.diff;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.Label;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.util.Printer;
import org.objectweb.asm.util.Textifier;
import org.objectweb.asm.util.TraceClassVisitor;
import org.objectweb.asm.util.TraceMethodVisitor;

public class ContentPrinter
{
	private static Logger logger = Logger.getLogger(ContentPrinter.class);

	// print method node content
	public static String print(MethodNode node) {
		Printer printer = new Textifier(Opcodes.ASM5) {
			@Override
			public void visitLineNumber(int line, Label start) {
			}
		};
		TraceMethodVisitor methodPrinter = new TraceMethodVisitor(printer);
		node.accept(methodPrinter);
		StringWriter sw = new StringWriter();
		printer.print(new PrintWriter(sw));
		printer.getText().clear();
		// include the access code in case of access code changes
		String methodContent = node.access + "\n"+ node.signature+"\n"+ sw.toString();
		logger.debug("Method " + node.name + " content: " + methodContent);
		return methodContent;
	}

	// print class class file info
	public static String print(ClassNode node) {
		StringWriter sw = new StringWriter();
		TraceClassVisitor classPrinter = new TraceClassVisitor(
				new PrintWriter(sw));
		node.accept(classPrinter);
		logger.debug("Class " + node.name + " content: \n" + sw.toString());
		return sw.toString();
	}

	// print class header info (e.g., access flags, inner classes, etc)
	public static String printClassHeader(ClassNode node) {
		Printer printer = new Textifier(Opcodes.ASM5) {
			@Override
			public Textifier visitField(int access, String name, String desc,
					String signature, Object value) {
				return new Textifier();
			}

			@Override
			public Textifier visitMethod(int access, String name, String desc,
					String signature, String[] exceptions) {
				return new Textifier();
			}
		};
		StringWriter sw = new StringWriter();
		TraceClassVisitor classPrinter = new TraceClassVisitor(null, printer,
				new PrintWriter(sw));
		node.accept(classPrinter);

		return sw.toString();
	}

	public static String print(FieldNode node) {
		return node.value == null ? null : node.value.toString();
	}

}
