/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.diff;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.log4j.Logger;

import set.hyrts.utils.Properties;

/**
 * Compute the class level differences
 * 
 * @author lingmingzhang
 *
 */
public class VersionDiff
{
	private static Logger logger = Logger.getLogger(VersionDiff.class);

	public static Map<String, String> oldClasses = new HashMap<String, String>();
	public static Map<String, String> newClasses = new HashMap<String, String>();
	public static Map<String, String> oldClassHeaders = new HashMap<String, String>();
	public static Map<String, String> newClassHeaders = new HashMap<String, String>();
	public static Map<String, Map<String, String>> oldClassMeths = new HashMap<String, Map<String, String>>();
	public static Map<String, Map<String, String>> newClassMeths = new HashMap<String, Map<String, String>>();

	public static Set<String> deletedFiles = new HashSet<String>();
	public static Set<String> addedFiles = new HashSet<String>();
	public static Set<String> changedFiles = new HashSet<String>();
	public static Set<String> classHeaderChanges = new HashSet<String>();

	public static String OLDDIR;

	// transformed class changes for hybrid techniques
	public static Set<String> transformedClassChanges = new HashSet<String>();

	// adding instance method changes
	public static Set<String> AIMs = new HashSet<String>();
	// deleting instance method changes
	public static Set<String> DIMs = new HashSet<String>();
	// changing instance method changes
	public static Set<String> CIMs = new HashSet<String>();
	// adding static method changes
	public static Set<String> ASMs = new HashSet<String>();
	// deleting static method changes
	public static Set<String> DSMs = new HashSet<String>();
	// changing static method changes
	public static Set<String> CSMs = new HashSet<String>();
	// adding initializer changes
	public static Set<String> AIs = new HashSet<String>();
	// deleting initializer changes
	public static Set<String> DIs = new HashSet<String>();
	// changing initializer changes
	public static Set<String> CIs = new HashSet<String>();
	// adding static initializer changes
	public static Set<String> ASIs = new HashSet<String>();
	// deleting initializer changes
	public static Set<String> DSIs = new HashSet<String>();
	// changing static initializer changes
	public static Set<String> CSIs = new HashSet<String>();
	// method lookup changes
	public static Set<String> LCs = new HashSet<String>();

	// detailed block changes
	public static Set<String> BLKs = new HashSet<String>();

	public static boolean transDSI = false;
	public static boolean transASI = false;
	public static boolean transCSI = false;
	public static boolean transDI = false;
	public static boolean transAI = false;
	public static boolean transCI = false;
	public static boolean transDSM = false;
	public static boolean transASM = false;
	public static boolean transCSM = false;
	public static boolean transDIM = false;
	public static boolean transAIM = false;
	public static boolean transCIM = false;
	// public static final int CL_LEVEL = 10;

	static void clearContents() {
		oldClasses.clear();
		newClasses.clear();
		oldClassMeths.clear();
		newClassMeths.clear();
		oldClassHeaders.clear();
		newClassHeaders.clear();
		ClassInheritanceGraph.clear();
	}

	static void clearChanges() {
		deletedFiles.clear();
		addedFiles.clear();
		changedFiles.clear();
		AIMs.clear();
		DIMs.clear();
		CIMs.clear();
		ASMs.clear();
		DSMs.clear();
		CSMs.clear();
		AIs.clear();
		DIs.clear();
		CIs.clear();
		ASIs.clear();
		DSIs.clear();
		CSIs.clear();
		LCs.clear();
		BLKs.clear();
		transformedClassChanges.clear();
		classHeaderChanges.clear();
	}

	// used when want to compute only the changes without selection
	public static void main(String[] args) throws Exception {
		String oldDir = args[0];
		String newDir = args[1];
		String newClassPath = args[2];
		Properties.TRACER_COV_TYPE = Properties.METH_COV;
		Properties.FILE_CHECKSUM = Properties.RTSVariant.HyRTS + "-checksum";
		compute(oldDir, newDir, newClassPath);
	}

	// compute the version diff
	public static void compute(String oldDir, String newDir,
			String newClassPath) throws Exception {
		OLDDIR = oldDir;
		clearContents();
		clearChanges();

		if (oldDir != null)
			ClassContentParser.deserializeOldContents(oldDir);
		ClassContentParser.parseAndSerializeNewContents(
				parseClassPath(newClassPath), newDir);
		diff();
		clearContents();
		System.out.println(Properties.HYRTS_TAG + "Extracted changes: "
				+ deletedFiles.size() + " " + addedFiles.size() + " "
				+ changedFiles.size() + " " + classHeaderChanges.size() + " "
				+ DSIs.size() + " " + ASIs.size() + " " + CSIs.size() + " "
				+ DIs.size() + " " + AIs.size() + " " + CIs.size() + " "
				+ DSMs.size() + " " + ASMs.size() + " " + CSMs.size() + " "
				+ DIMs.size() + " " + AIMs.size() + " " + CIMs.size() + " "
				+ getCorrespondingClassNum(DSIs) + " "
				+ getCorrespondingClassNum(ASIs) + " "
				+ getCorrespondingClassNum(CSIs) + " "
				+ getCorrespondingClassNum(DIs) + " "
				+ getCorrespondingClassNum(AIs) + " "
				+ getCorrespondingClassNum(CIs) + " "
				+ getCorrespondingClassNum(DSMs) + " "
				+ getCorrespondingClassNum(ASMs) + " "
				+ getCorrespondingClassNum(CSMs) + " "
				+ getCorrespondingClassNum(DIMs) + " "
				+ getCorrespondingClassNum(AIMs) + " "
				+ getCorrespondingClassNum(CIMs));

		for (String file : addedFiles) {
			logger.info("Added files: " + file);
		}
		for (String file : changedFiles) {
			logger.info("Changed files: " + file);
		}
		for (String file : deletedFiles) {
			logger.info("Deleted files: " + file);
		}
		for (String method : ASMs) {
			logger.info("Added static method: " + method);
		}
		for (String method : CSMs) {
			logger.info("Changed static method: " + method);
		}
		for (String method : DSMs) {
			logger.info("Deleted static method: " + method);
		}
		for (String method : AIMs) {
			logger.info("Added instance method: " + method);
		}
		for (String method : CIMs) {
			logger.info("Changed instance method: " + method);
		}
		for (String method : DIMs) {
			logger.info("Deleted instance method: " + method);
		}
		for (String method : LCs) {
			logger.info("Method lookup changes: " + method);
		}
		for (String blk : BLKs) {
			System.out.println(
					Properties.HYRTS_TAG + " Basic block changes: " + blk);
		}
	}

	public static String getRatio(int top, int bottom) {
		if (bottom == 0)
			return "0.00%";
		DecimalFormat format = new DecimalFormat("0.00");
		return format.format(top * 100 / bottom) + "%";
	}

	public static int getCorrespondingClassNum(Set<String> input) {
		Set<String> output = new HashSet<String>();
		for (String in : input) {
			String[] items = in.split(":");
			output.add(items[0]);
		}
		return output.size();
	}

	public static Set<ClassFileHandler> parseClassPath(String path)
			throws Exception {
		Set<ClassFileHandler> result = new HashSet<ClassFileHandler>();
		String[] items = path.split(":");
		for (String item : items) {
			Set<ClassFileHandler> set = null;
			if (item.endsWith(".jar")) {
				set = parseJarFile(item);
			} else {
				set = parseClassDir(item);
			}
			result.addAll(set);
			set.clear();
		}
		if (result.size() == 0)
			System.out.println(Properties.HYRTS_TAG
					+ "No class files under analysis: property \""
					+ Properties.NEW_CLASSPATH_KEY
					+ "\" must be set correctly");
		return result;
	}

	// get checksum info for each class file
	public static Set<ClassFileHandler> parseJarFile(String jarPath)
			throws IOException, Exception {
		JarFile f = new JarFile(jarPath);
		Set<ClassFileHandler> result = new HashSet<ClassFileHandler>();
		Enumeration<JarEntry> entries = f.entries();
		while (entries.hasMoreElements()) {
			JarEntry entry = entries.nextElement();

			String entryName = entry.getName();
			if (entryName.endsWith(".class")) {
				String className = entryName.replace(".class", "");
				ClassFileHandler file = new ClassFileHandler(className, f);
				result.add(file);
			}
		}
		return result;
	}

	public static Set<ClassFileHandler> parseClassDir(String dir)
			throws IOException, Exception {
		Set<ClassFileHandler> result = new HashSet<ClassFileHandler>();

		File dirFile = new File(dir);
		List<File> workList = new ArrayList<File>();
		workList.add(dirFile);
		while (!workList.isEmpty()) {
			File curF = workList.remove(0);
			if (curF.getName().endsWith(".class")) {
				String className = getClassName(dir, curF);
				ClassFileHandler file = new ClassFileHandler(className,
						curF.getAbsolutePath());
				result.add(file);
			} else if (curF.isDirectory()) {
				for (File f : curF.listFiles())
					workList.add(f);
			}
		}
		return result;
	}

	public static String getClassName(String dir, File classFile) {
		String absolutePath = classFile.getAbsolutePath();
		String className = absolutePath.substring(dir.length() + 1)
				.replace(".class", "");
		return className;
	}

	public static void diff() throws IOException {
		for (String clazz : VersionDiff.oldClasses.keySet()) {
			// logger.debug("diff class: " + clazz);
			if (!VersionDiff.newClasses.containsKey(clazz)) {
				deletedFiles.add(clazz);
			} else {
				if (!VersionDiff.oldClasses.get(clazz)
						.equals(VersionDiff.newClasses.get(clazz))) {
					changedFiles.add(clazz);
					if (!Properties.TRACER_COV_TYPE
							.endsWith(Properties.CLASS_COV))
						AtomicLevelDiff.diff(clazz);
				}
			}
			VersionDiff.newClasses.remove(clazz);
		}
		for (String file : VersionDiff.newClasses.keySet()) {
			addedFiles.add(file);
		}
	}
}
