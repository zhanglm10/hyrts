/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.diff;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FrameNode;
import org.objectweb.asm.tree.IincInsnNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.IntInsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.LdcInsnNode;
import org.objectweb.asm.tree.LookupSwitchInsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.MultiANewArrayInsnNode;
import org.objectweb.asm.tree.TableSwitchInsnNode;
import org.objectweb.asm.tree.TryCatchBlockNode;
import org.objectweb.asm.tree.TypeInsnNode;
import org.objectweb.asm.tree.VarInsnNode;
import org.objectweb.asm.util.Printer;

/**
 * CFG-level change analysis
 * 
 * @author lingmingzhang
 *
 */
public final class CFGDiff
{

	Map<Integer, Node> id_node;
	Node curNode;
	static Logger logger = Logger.getLogger(CFGDiff.class);

	/**
	 * Data structure for holding CFG nodes
	 */
	static class Node
	{
		int id;
		List<Node> succ = new ArrayList<Node>();
		String content = "";

		public Node(int id) {
			this.id = id;
		}

		public void append(String con) {
			content = content + con + " ";
		}

		public void newLine() {
			content = content + "\n";
		}

		public void addSuccNode(Node node) {
			if (!succ.contains(node))
				succ.add(node);
		}

		public boolean equals(Node node) {
			return this.content.equals(node.content);
		}
	}
	/*
	 * public static void main(final String[] args) throws Exception { // create
	 * a ClassReader that loads the Java .class file specified as the // command
	 * line argument final String classFileName =
	 * "/Users/lingmingzhang/Documents/workspace/hyrts-maven/target/test-classes/set/hyrts/example/test/A.class"
	 * ;// args[0]; final ClassNode clazz = parseClass(classFileName); // create
	 * a dumper and have it dump the given ClassNode final CFGDiff dumper = new
	 * CFGDiff(); //dumper.parseClass(clazz); }
	 */

	/**
	 * Perform detailed CFG-level change analysis
	 * 
	 * @param oldPath
	 * @param newPath
	 * @param method
	 * @return
	 * @throws IOException
	 * @throws IOException
	 */
	public Set<String> diff(String oldPath, String newPath, String method)
			throws IOException, IOException {
		ClassNode oldNode = parseClass(oldPath);
		MethodNode oldMeth = null, newMeth = null;
		for (MethodNode m : (List<MethodNode>) oldNode.methods) {
			if (ClassContentParser.methodName(m).equals(method)) {
				oldMeth = m;
				break;
			}
		}
		ClassNode newNode = parseClass(newPath);
		for (MethodNode m : (List<MethodNode>) newNode.methods) {
			if (ClassContentParser.methodName(m).equals(method)) {
				newMeth = m;
				break;
			}
		}
		Set<String> chgs = compareCFG(oldNode.name, oldMeth, newMeth);
		return chgs;
	}

	/**
	 * Parse the corresponding class file, and return the ASM ClassNode.
	 * 
	 * @param classFileName
	 * @return
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private static ClassNode parseClass(final String classFileName)
			throws IOException, FileNotFoundException {
		final ClassReader cr = new ClassReader(
				new FileInputStream(classFileName));
		// create an empty ClassNode (in-memory representation of a class)
		final ClassNode clazz = new ClassNode();
		// have the ClassReader read the class file and populate the ClassNode
		// with the corresponding information
		cr.accept(clazz, 0);
		return clazz;
	}

	/**
	 * Compare the two method nodes and find changed basic blocks.
	 * 
	 * @param className
	 * @param m1
	 * @param m2
	 * @return
	 */
	public Set<String> compareCFG(String className, MethodNode m1,
			MethodNode m2) {
		Set<String> chgs = new HashSet<String>();
		// if the method signature is already changed, directly return diff at
		// block 0
		if (m1.access != m2.access || !StringUtils.equals(m1.signature, m2.signature) ) {
			chgs.add(
					className + ":" + ClassContentParser.methodName(m1) + ":0");
			return chgs;
		}
		Map<Integer, Node> graph1 = parseMethod(m1);
		Map<Integer, Node> graph2 = parseMethod(m2);
		Node root1 = graph1.get(0);
		Node root2 = graph2.get(0);
		Set<Node> visited = new HashSet<Node>();
		Set<Integer> diffIDs = new HashSet<Integer>();
		DFSDiff(root1, root2, visited, diffIDs);
		Map<Integer, Integer> labelMap = labelMapping(m1);
		for (Integer id : diffIDs) {
			chgs.add(className + ":" + ClassContentParser.methodName(m1) + ":"
					+ labelMap.get(id));
		}
		return chgs;
	}

	/**
	 * Perform the Depth-First Search to compare basic blocks of two methods
	 * 
	 * @param n1
	 * @param n2
	 * @param visited
	 * @param chgs
	 */
	public void DFSDiff(Node n1, Node n2, Set<Node> visited,
			Set<Integer> chgs) {
		if (visited.contains(n1)) {
			return;
		}
		visited.add(n1);
		if (!n1.equals(n2) || n1.succ.size() != n2.succ.size()) {
			chgs.add(n1.id);
		} else {
			for (int i = 0; i < n1.succ.size(); i++) {
				DFSDiff(n1.succ.get(i), n2.succ.get(i), visited, chgs);
			}
		}
	}

	/**
	 * Map instruction offset to corresponding label ID (0 and onwards).
	 * 
	 * @param method
	 * @return
	 */
	public Map<Integer, Integer> labelMapping(final MethodNode method) {
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		// get the list of all instructions in that method
		final InsnList instructions = method.instructions;
		int labelId = 0;
		for (int i = 0; i < instructions.size(); i++) {
			final AbstractInsnNode instruction = instructions.get(i);
			if (instruction.getType() == AbstractInsnNode.LABEL) {
				map.put(i, labelId++);
			}
		}
		return map;
	}

	/**
	 * Build the CFG for a method
	 * 
	 * @param method
	 * @return
	 */
	public Map<Integer, Node> parseMethod(final MethodNode method) {
		// System.out.println(" Method: " + method.name + method.desc);
		// get the list of all instructions in that method
		final InsnList instructions = method.instructions;
		id_node = new LinkedHashMap<Integer, Node>();
		curNode = new Node(0);
		id_node.put(0, curNode);

		for (int i = 0; i < instructions.size(); i++) {
			final AbstractInsnNode instruction = instructions.get(i);
			analyzeInstruction(instruction, i, instructions);
		}

		// handle exceptions
		List<TryCatchBlockNode> trys = method.tryCatchBlocks;
		List<Integer> orderedLabels = new ArrayList<Integer>();
		if (trys != null && trys.size() > 0) {
			for (int i = 0; i < instructions.size(); i++) {
				if (id_node.containsKey(i)) {
					orderedLabels.add(i);
				}
			}
		}
		for (TryCatchBlockNode t : trys) {
			if (t.type == null)
				continue;
			int startIndex = instructions.indexOf(t.start);
			int handlerIndex = instructions.indexOf(t.handler);

			// System.out.println("id_node: " + id_node);
			// System.out.println(">>" + startIndex + " " + handlerIndex);
			// obtain the label ID just above the startIndex
			if (!id_node.containsKey(startIndex)) {
				int id = -1;
				for (int i : orderedLabels) {
					if (i > startIndex) {
						startIndex = id;
						break;
					} else {
						id = i;
					}
				}
			}
			if (id_node.containsKey(handlerIndex)) {
				id_node.get(startIndex).addSuccNode(id_node.get(handlerIndex));
			}
		}

		logger.info(ClassContentParser.methodName(method));
		for (int i : id_node.keySet()) {
			Node n = id_node.get(i);
			logger.info(n.content);
			String item = i + "==>";
			for (Node next : n.succ)
				item += next.id + ",";
			logger.info(item);
		}
		return id_node;
	}

	/**
	 * Analyze each instruction to construct the corresponding CFG
	 * 
	 * @param instruction
	 * @param i
	 * @param instructions
	 */
	public void analyzeInstruction(final AbstractInsnNode instruction,
			final int i, final InsnList instructions) {
		String insns = "";
		final int opcode = instruction.getOpcode();
		final String mnemonic = opcode == -1 ? ""
				: Printer.OPCODES[instruction.getOpcode()];
		insns += i + ":\t" + mnemonic + " ";
		curNode.append(mnemonic);
		// There are different subclasses of AbstractInsnNode.
		// AbstractInsnNode.getType() represents the subclass as an int.
		// Note:
		// to check the subclass of an instruction node, we can either use:
		// if (instruction.getType()==AbstractInsnNode.LABEL)
		// or we can use:
		// if (instruction instanceof LabelNode)
		// They give the same result, but the first one can be used in a switch
		// statement.
		switch (instruction.getType()) {
		case AbstractInsnNode.LABEL:
			// pseudo-instruction (branch or exception target)
			insns += ("// label");
			// beginning of basic block: after jump insn or at label
			if (id_node.containsKey(i)) {
				// ending of basic block: before label
				if (i > 0) {
					AbstractInsnNode insn = instructions.get(i - 1);
					if (insn.getType() != AbstractInsnNode.JUMP_INSN) {
						int insnCode = insn.getOpcode();
						if (insnCode != Opcodes.RETURN
								&& insnCode != Opcodes.ARETURN
								&& insnCode != Opcodes.DRETURN
								&& insnCode != Opcodes.FRETURN
								&& insnCode != Opcodes.IRETURN
								&& insnCode != Opcodes.LRETURN)
							curNode.addSuccNode(id_node.get(i));
					}
				}
				curNode = id_node.get(i);
			}
			break;
		case AbstractInsnNode.FRAME:
			// pseudo-instruction (stack frame map)
			// insns += ("// stack frame map");
			insns += ((FrameNode) instruction).stack;
			break;
		case AbstractInsnNode.LINE:
			// pseudo-instruction (line number information)
			insns += ("// line number information");
		case AbstractInsnNode.INSN:
			// Opcodes: NOP, ACONST_NULL, ICONST_M1, ICONST_0, ICONST_1,
			// ICONST_2,
			// ICONST_3, ICONST_4, ICONST_5, LCONST_0, LCONST_1, FCONST_0,
			// FCONST_1, FCONST_2, DCONST_0, DCONST_1, IALOAD, LALOAD, FALOAD,
			// DALOAD, AALOAD, BALOAD, CALOAD, SALOAD, IASTORE, LASTORE,
			// FASTORE,
			// DASTORE, AASTORE, BASTORE, CASTORE, SASTORE, POP, POP2, DUP,
			// DUP_X1, DUP_X2, DUP2, DUP2_X1, DUP2_X2, SWAP, IADD, LADD, FADD,
			// DADD, ISUB, LSUB, FSUB, DSUB, IMUL, LMUL, FMUL, DMUL, IDIV, LDIV,
			// FDIV, DDIV, IREM, LREM, FREM, DREM, INEG, LNEG, FNEG, DNEG, ISHL,
			// LSHL, ISHR, LSHR, IUSHR, LUSHR, IAND, LAND, IOR, LOR, IXOR, LXOR,
			// I2L, I2F, I2D, L2I, L2F, L2D, F2I, F2L, F2D, D2I, D2L, D2F, I2B,
			// I2C, I2S, LCMP, FCMPL, FCMPG, DCMPL, DCMPG, IRETURN, LRETURN,
			// FRETURN, DRETURN, ARETURN, RETURN, ARRAYLENGTH, ATHROW,
			// MONITORENTER, or MONITOREXIT.
			// zero operands, nothing to print
			if (instruction.getOpcode() == Opcodes.ATHROW) {
				Node n;
				if (!id_node.containsKey(i + 1)
						&& (i + 1) < instructions.size()) {
					n = new Node(i + 1);
					id_node.put(i + 1, n);
				}
			}
			break;
		case AbstractInsnNode.INT_INSN:
			// Opcodes: NEWARRAY, BIPUSH, SIPUSH.
			if (instruction.getOpcode() == Opcodes.NEWARRAY) {
				// NEWARRAY
				insns += (Printer.TYPES[((IntInsnNode) instruction).operand]);
				curNode.append(
						Printer.TYPES[((IntInsnNode) instruction).operand]);
			} else {
				// BIPUSH or SIPUSH
				insns += (((IntInsnNode) instruction).operand);
				curNode.append(((IntInsnNode) instruction).operand + "");
			}
			break;
		case AbstractInsnNode.JUMP_INSN:
		// Opcodes: IFEQ, IFNE, IFLT, IFGE, IFGT, IFLE, IF_ICMPEQ,
		// IF_ICMPNE, IF_ICMPLT, IF_ICMPGE, IF_ICMPGT, IF_ICMPLE, IF_ACMPEQ,
		// IF_ACMPNE, GOTO, JSR, IFNULL or IFNONNULL.
		{
			final LabelNode targetInstruction = ((JumpInsnNode) instruction).label;
			final int targetId = instructions.indexOf(targetInstruction);
			insns += (targetId);

			// ending of a basic block: at the jump insn
			Node n1;
			if (id_node.containsKey(targetId)) {
				n1 = id_node.get(targetId);
			} else {
				n1 = new Node(targetId);
				id_node.put(targetId, n1);
			}
			curNode.addSuccNode(n1);

			Node n2;
			if (id_node.containsKey(i + 1)) {
				n2 = id_node.get(i + 1);
			} else {
				n2 = new Node(i + 1);
				id_node.put(i + 1, n2);
			}
			if (instruction.getOpcode() != Opcodes.GOTO
					&& instruction.getOpcode() != Opcodes.JSR) {
				curNode.addSuccNode(n2);
			}
			break;
		}
		case AbstractInsnNode.LDC_INSN:
			// Opcodes: LDC.
			insns += (((LdcInsnNode) instruction).cst);
			curNode.append(((LdcInsnNode) instruction).cst + "");
			break;
		case AbstractInsnNode.IINC_INSN:
			// Opcodes: IINC.
			insns += (((IincInsnNode) instruction).var);
			insns += (" ");
			insns += (((IincInsnNode) instruction).incr);
			curNode.append(((IincInsnNode) instruction).var + " "
					+ ((IincInsnNode) instruction).incr);
			break;
		case AbstractInsnNode.TYPE_INSN:
			// Opcodes: NEW, ANEWARRAY, CHECKCAST or INSTANCEOF.
			insns += (((TypeInsnNode) instruction).desc);
			curNode.append(((TypeInsnNode) instruction).desc);
			break;
		case AbstractInsnNode.VAR_INSN:
			// Opcodes: ILOAD, LLOAD, FLOAD, DLOAD, ALOAD, ISTORE,
			// LSTORE, FSTORE, DSTORE, ASTORE or RET.
			insns += (((VarInsnNode) instruction).var);
			curNode.append(((VarInsnNode) instruction).var + "");
			break;
		case AbstractInsnNode.FIELD_INSN:
			// Opcodes: GETSTATIC, PUTSTATIC, GETFIELD or PUTFIELD.
			insns += (((FieldInsnNode) instruction).owner);
			insns += (".");
			insns += (((FieldInsnNode) instruction).name);
			insns += (" ");
			insns += (((FieldInsnNode) instruction).desc);
			curNode.append(((FieldInsnNode) instruction).owner + "."
					+ ((FieldInsnNode) instruction).name + " "
					+ ((FieldInsnNode) instruction).desc);
			break;
		case AbstractInsnNode.METHOD_INSN:
			// Opcodes: INVOKEVIRTUAL, INVOKESPECIAL, INVOKESTATIC,
			// INVOKEINTERFACE or INVOKEDYNAMIC.
			insns += (((MethodInsnNode) instruction).owner);
			insns += (".");
			insns += (((MethodInsnNode) instruction).name);
			insns += (" ");
			insns += (((MethodInsnNode) instruction).desc);
			curNode.append(((MethodInsnNode) instruction).owner + "."
					+ ((MethodInsnNode) instruction).name + " "
					+ ((MethodInsnNode) instruction).desc);
			break;
		case AbstractInsnNode.MULTIANEWARRAY_INSN:
			// Opcodes: MULTIANEWARRAY.
			insns += (((MultiANewArrayInsnNode) instruction).desc);
			insns += (" ");
			insns += (((MultiANewArrayInsnNode) instruction).dims);
			curNode.append(((MultiANewArrayInsnNode) instruction).desc + " "
					+ ((MultiANewArrayInsnNode) instruction).dims);
			break;
		// ending of basic block: at jump insn
		case AbstractInsnNode.LOOKUPSWITCH_INSN:
		// Opcodes: LOOKUPSWITCH.
		{
			final List keys = ((LookupSwitchInsnNode) instruction).keys;
			final List labels = ((LookupSwitchInsnNode) instruction).labels;
			for (int t = 0; t < keys.size(); t++) {
				final int key = (Integer) keys.get(t);
				final LabelNode targetInstruction = (LabelNode) labels.get(t);
				final int targetId = instructions.indexOf(targetInstruction);
				insns += (key + ": " + targetId + ", ");

				curNode.append(key + "");
				Node n;
				if (id_node.containsKey(targetId)) {
					n = id_node.get(targetId);
				} else {
					n = new Node(targetId);
					id_node.put(targetId, n);
				}
				curNode.addSuccNode(n);
			}
			final LabelNode defaultTargetInstruction = ((LookupSwitchInsnNode) instruction).dflt;
			final int defaultTargetId = instructions
					.indexOf(defaultTargetInstruction);
			insns += ("default: " + defaultTargetId);

			curNode.append("default");
			Node n;
			if (id_node.containsKey(defaultTargetId)) {
				n = id_node.get(defaultTargetId);
			} else {
				n = new Node(defaultTargetId);
				id_node.put(defaultTargetId, n);
			}
			curNode.addSuccNode(n);
			break;
		}
		case AbstractInsnNode.TABLESWITCH_INSN:
		// Opcodes: TABLESWITCH.
		{
			final int minKey = ((TableSwitchInsnNode) instruction).min;
			final List labels = ((TableSwitchInsnNode) instruction).labels;
			for (int t = 0; t < labels.size(); t++) {
				final int key = minKey + t;
				final LabelNode targetInstruction = (LabelNode) labels.get(t);
				final int targetId = instructions.indexOf(targetInstruction);
				insns += (key + ": " + targetId + ", ");

				curNode.append(key + "");
				Node n;
				if (id_node.containsKey(targetId)) {
					n = id_node.get(targetId);
				} else {
					n = new Node(targetId);
					id_node.put(targetId, n);
				}
				curNode.addSuccNode(n);
			}
			final LabelNode defaultTargetInstruction = ((TableSwitchInsnNode) instruction).dflt;
			final int defaultTargetId = instructions
					.indexOf(defaultTargetInstruction);
			insns += ("default: " + defaultTargetId);

			curNode.append("default");
			Node n;
			if (id_node.containsKey(defaultTargetId)) {
				n = id_node.get(defaultTargetId);
			} else {
				n = new Node(defaultTargetId);
				id_node.put(defaultTargetId, n);
			}
			curNode.addSuccNode(n);
			break;
		}
		}
		logger.debug(insns);
	}

}
