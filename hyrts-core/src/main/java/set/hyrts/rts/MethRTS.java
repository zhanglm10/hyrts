/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.hyrts.rts;

import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.common.io.Files;

import set.hyrts.coverage.io.TracerIO;
import set.hyrts.coverage.junit.FTracerJUnitUtils;
import set.hyrts.diff.VersionDiff;
import set.hyrts.diff.traditional.TradMethVersionDiff;
import set.hyrts.utils.Properties;

/**
 * Selecting affected test classes via traditional method-level regression test selection
 * (e.g., FaultTracer and Chianti)
 * 
 * @author lingmingzhang
 *
 */
public class MethRTS
{
	public final static String CLASS = "set/hyrts/rts/MethRTS";
	private static Logger logger = Logger.getLogger(MethRTS.class);

	public static Set<String> main() throws Exception {
		// if(true)return;
		long startTime = System.currentTimeMillis();

		Set<String> excluded = new HashSet<String>();
		// compute version diff
		TradMethVersionDiff.compute(Properties.OLD_DIR, Properties.NEW_DIR,
				Properties.NEW_CLASSPATH);

		// if old cov is not available, directly rerun all tests
		if (Properties.OLD_DIR == null) {
			System.out.println(Properties.HYRTS_TAG
					+ "No RTS analysis due to no old coverage, but is computing coverage info and checksum info for future RTS...");
			return excluded;
		}

		// obtain old cov info
		Map<String, Set<String>> old_cov_map = TracerIO
				.loadCovFromDirectory(Properties.OLD_DIR);
		// if no prior version cov, directly return without any exclusion
		if (old_cov_map == null)
			return excluded;

		for (String test : old_cov_map.keySet()) {
			// select changed tests
			if (!TradMethVersionDiff.changedFiles
					.contains(test.replace(".", "/"))
					&& !isAffected(old_cov_map.get(test))) {
				excluded.add(test);
				if (!Properties.OLD_DIR.equals(Properties.NEW_DIR)) {
					File oldV = new File(
							TracerIO.getTestCovFilePath(Properties.OLD_DIR, test));
					File newV = new File(
							TracerIO.getTestCovFilePath(Properties.NEW_DIR, test));
					Files.copy(oldV, newV);
				}
			}
		}
		long endTime = System.currentTimeMillis();

		// writeExcluded(excludedTests);
		System.out.println(Properties.HYRTS_TAG + "RTS excluded "
				+ excluded.size() + " out of "
				+ old_cov_map.keySet().size() + " test classes using "
				+ (endTime - startTime) + "ms ");
		return excluded;
	}

	public static boolean isAffected(Set<String> testCov) {
		logger.debug("Meth dependency: " + testCov);

		// handle static initializer changes
		for (String cm : TradMethVersionDiff.CSIs) {
			if (testCov.contains(cm))
				return true;
		}
		// handle static initializer deletions
		for (String cm : TradMethVersionDiff.DSIs) {
			if (testCov.contains(cm))
				return true;
		}
		// handle initializer changes
		for (String cm : TradMethVersionDiff.CIs) {
			if (testCov.contains(cm))
				return true;
		}
		// handle initializer deletions
		for (String cm : TradMethVersionDiff.DIs) {
			if (testCov.contains(cm))
				return true;
		}
		// handle method changes
		for (String cm : TradMethVersionDiff.CIMs) {
			if (testCov.contains(cm))
				return true;
		}
		for (String cm : TradMethVersionDiff.CSMs) {
			if (testCov.contains(cm))
				return true;
		}
		// handle static method deletions
		for (String cm : TradMethVersionDiff.DSMs) {
			if (testCov.contains(cm))
				return true;
		}
		// handle method lookup changes for instance method deletions or
		// additions
		for (String cm : TradMethVersionDiff.LCs) {
			if (testCov.contains(cm))
				return true;
		}
		return false;
	}

}
