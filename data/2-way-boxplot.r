library(ggplot2)
library(reshape2)
library(wesanderson)

unorderedSubs<-c("invokebinder", "compile-testing", "commons-jxpath", "commons-fileupload", "joda-time", "commons-functor", "la4j",
"commons-dbutils", "logstash-logback-encoder",
"commons-validator", "asterisk-java", "commons-cli",
"commons-email", "jfreechart", "commons-codec",
"commons-collections", "commons-compress", "commons-lang",
"commons-configuration", "commons-imaging", "commons-net",
"java-apns", "commons-dbcp", "log4j", "closure-compiler",
"HikariCP", "commons-math", "commons-io", "stream-lib",
"OpenTripPlanner", "commons-pool", "mapdb")
subs<-c("invokebinder", "compile-testing", "logstash-logback-encoder", "commons-cli", "joda-time", "commons-dbutils", "commons-validator", "commons-fileupload", "asterisk-java", "commons-functor", "la4j", "commons-jxpath", "commons-email", "commons-compress", "commons-codec", "jfreechart", "commons-collections", "commons-lang", "commons-imaging", "commons-configuration", "commons-net", "closure-compiler", "java-apns", "commons-io", "commons-math", "commons-dbcp", "log4j", "stream-lib", "HikariCP", "OpenTripPlanner", "commons-pool", "mapdb")

figWidth<-10
figHeight<-6.5
textFont<-15
techs<-c("HRTS", "FRTS")
paperDir<-"fse17-results/"


pdf(paste(c(paperDir,"fig3-testNum.pdf"),collapse=""), width=figWidth, height=figHeight)
t<-read.table("fse17-results/rts-testNum.dat", header=T)
t$Sub<-as.factor(t$Sub)
t$Tech<-as.factor(t$Tech)
p<-ggplot(data=subset(t, (Tech==0|Tech==14)&Tag=="false"), aes(x=Sub, y=TestNum, fill = Tech)) + xlab("Subs") + ylab("Selected Test Number (%)")+geom_boxplot()+theme(text = element_text(size=textFont),axis.text.x = element_text(angle = 30, hjust = 1), legend.position=c(.9,.9))+scale_x_discrete(limits=subs)+scale_fill_grey(name="Compared:", breaks=c("0", "14"), labels=techs)+stat_summary(fun.y="mean",aes(group=Tech),position=position_dodge(width=0.75),shape=23,size=2,fill="white",col='black',geom='point')
print(p)
garbage <-dev.off()

pdf(paste(c(paperDir,"fig3-aeTime.pdf"),collapse=""), width=figWidth, height=figHeight)
t<-read.table("fse17-results/rts-time.dat", header=T)
t$Sub<-as.factor(t$Sub)
t$Tech<-as.factor(t$Tech)
p<-ggplot(data=subset(t, (Tech==0|Tech==14)&Tag=="true"), aes(x=Sub, y=Ratio, fill = Tech)) + xlab("Subs") + ylab("Selected Test AE Time (%)")+coord_cartesian(ylim = c(0, 200))+geom_boxplot()+theme(text = element_text(size=textFont),axis.text.x = element_text(angle = 30, hjust = 1), legend.position=c(.9,.9))+scale_x_discrete(limits=subs)+scale_fill_grey(name="Compared:", breaks=c("0", "14"), labels=techs)+stat_summary(fun.y="mean",aes(group=Tech),position=position_dodge(width=0.75),shape=23,size=2,fill="white",col='black',geom='point')
print(p)
garbage <-dev.off()


pdf(paste(c(paperDir,"fig3-aecTime.pdf"),collapse=""), width=figWidth, height=figHeight)
t<-read.table("fse17-results/rts-time.dat", header=T)
t$Sub<-as.factor(t$Sub)
t$Tech<-as.factor(t$Tech)
p<-ggplot(data=subset(t, (Tech==0|Tech==14)&Tag=="false"), aes(x=Sub, y=Ratio, fill = Tech)) + xlab("Subs") + ylab("Selected Test AEC Time (%)")+coord_cartesian(ylim = c(0, 200)) +geom_boxplot()+theme(text = element_text(size=textFont),axis.text.x = element_text(angle = 30, hjust = 1), legend.position=c(.9,.9))+scale_x_discrete(limits=subs)+scale_fill_grey(name="Compared:", breaks=c("0", "14"), labels=techs)+stat_summary(fun.y="mean",aes(group=Tech),position=position_dodge(width=0.75),shape=23,size=2,fill="white",col='black',geom='point')
print(p)
garbage <-dev.off()
