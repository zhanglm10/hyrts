# Data/Scripts for Paper "Hybrid Regression Test Selection" #


### Environment ###

* Linux OS
* R with Scripting Front-End (Rscript) 
* ggplot2 library via `install.packages("ggplot2")`

### Result Reproduction ###

* `reproduction-main.sh`: the main script for reproducing all the
  tables/figs in the paper based on the stored raw data. Note that the script may take over 20 minutes to finish.

  + `printDiffTable.sh`: shell script to print change statistics (table 4)

  + `2-way-boxplot.r`: R script to draw fig 3.

  + `testNum-study.r`: R script to present the study results in terms of selected test ratio (table 5) and perform statistical analysis

  + `time-study.r`: R script to present the study results in terms of AE (offline mode) time (table 6) and perform statistical analysis

  + `refinedTableComp.r`: R script to present the HRTSf and HRTSb results (table 7) and perform statistical analysis

* Input: The corresponding raw data (including the versions used) for each project are stored under the
  directories with the project name

* Output: All the generated data/tables/figs will be stored under directory "fse17-results".

