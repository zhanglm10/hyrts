#!/bin/bash

DIFF=diff-detailed.res
CHGTYPE=16

readChanges () {
v=0;
while read -a matrix$v; do
    let v++;
done < $1
}

printLine () {
printf "$2">>$1;
arr=(${!3});
for((i=0;i<CHGTYPE;i++)); do
    if [ $(echo "${arr[$i]}>0"|bc) -eq 0 ]; then
	printf "&%'.2f \\\cellcolor{GrayTwo}" ${arr[$i]}>>$1;
    else
	printf "&%'.2f " ${arr[$i]}>>$1;
    fi
done
printf "\\\\\\ \n" >>$1;
}

OLDSUBS=("invokebinder" "compile-testing" "commons-jxpath" "commons-fileupload" "joda-time" "commons-functor" "la4j" "commons-dbutils" "logstash-logback-encoder" "commons-validator" "asterisk-java" "commons-cli" "commons-email" "jfreechart" "commons-codec" "commons-collections" "commons-compress" "commons-lang" "commons-configuration" "commons-imaging" "commons-net" "java-apns" "commons-dbcp" "log4j" "closure-compiler" "HikariCP" "commons-math" "commons-io" "stream-lib" "OpenTripPlanner" "commons-pool" "mapdb")

SUBS=("invokebinder" "compile-testing" "logstash-logback-encoder" "commons-cli" "joda-time" "commons-dbutils" "commons-validator" "commons-fileupload" "asterisk-java" "commons-functor" "la4j" "commons-jxpath" "commons-email" "commons-compress" "commons-codec" "jfreechart" "commons-collections" "commons-lang" "commons-imaging" "commons-configuration" "commons-net" "closure-compiler" "java-apns" "commons-io" "commons-math" "commons-dbcp" "log4j" "stream-lib" "HikariCP" "OpenTripPlanner" "commons-pool" "mapdb")

TABLE=$(pwd)"/fse17-results/table4.tex"
SEP="&\t";
rm $TABLE
printf "\\\begin{table*}\n\\\center\\\small\n">>$TABLE
printf "\\\begin{adjustbox}{width=\\\textwidth}\n">>$TABLE
printf "\\\begin{tabular}{|l|rrr||r|rrr|rrr|rrr|rrr|}\n\\\hline\n">>$TABLE
printf "Subs & \\\multicolumn{3}{c||}{\\\# \\\Filelevel{} Changes} & \\\multicolumn{13}{c}{\\\# Fine-grained Changes}\\\\\\ \n\\\cline{2-17}\n">>$TABLE
printf " &\\\DF&\\\AF&\\\CF &\\\CH&{\\\DSI}&{\\\ASI}&{\\\CSI}&{\\\DI}&{\\\AI}&{\\\CI}&{\\\DSM}&{\\\ASM}&{\\\CSM}&{\\\DIM}&{\\\AIM}&{\\\CIM}\\\\\\ \\\hline\\\hline\n">>$TABLE
# print statistics for all subjects

for((i=0;i<CHGTYPE;i++)); do
    avgAvg[$i]=0;
done

subCount=0;
for SUB in ${SUBS[@]}
do
cd $SUB;
readChanges <(cat $DIFF | sed "s|.*: \(.*\)|\1|" | sed "/0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0/d");
for((i=0;i<CHGTYPE;i++)); do
    sum[$i]=0;
    for((j=0;j<=v;j++)); do
	sum[$i]=$((sum[i]+matrix$j[i]));
    done
    avg[$i]=$(echo "scale=2;${sum[i]}/$((v+1))"|bc);
    avgAvg[$i]=$(echo "${avgAvg[i]}+${avg[i]}"|bc);
done
printLine $TABLE $SUB avg[@];
if [ $SUB == "commons-configuration" ]; then
    printf "\\\hline \n">>$TABLE;
    fi
cd ..;
let subCount++;
#break;
done
printf "\\\hline\n" >> $TABLE;
for((i=0;i<CHGTYPE;i++)); do
    avgAvg[$i]=$(echo "scale=2;${avgAvg[i]}/$subCount"|bc);
done
printLine $TABLE "{\\\bf Avg.}" avgAvg[@];
printf "\\\hline\\\end{tabular}\n">>$TABLE;
printf "\\\end{adjustbox}\n" >>$TABLE;
printf "\\\caption{\\\label{tab:change} Software change statistics}\n">>$TABLE;
printf "\\\end{table*}\n">>$TABLE;
