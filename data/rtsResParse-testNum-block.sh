#!/bin/bash

DIFF=" 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0"
JDKDIFF=" 0 23 1046 *"
# consider all versions or only changed ones
RANGE=$1
RTSRES=rts-block.res

# read raw rts.res file
readFile () {
v=-1;
while read line; do 
    
    if [[ $line == \[FaultTracer\]\ Old\ version\ location:* ]]; then
	OLDV="${line##* }";
	#echo $OLDV
    fi
    if [[ $line == \[FaultTracer\]\ Execution\ only\ config:* ]]; then
	EXEONLY=${line##* };
	#echo $EXEONLY
    fi
    if [[ $line == \[FaultTracer\]\ Extracted\ changes:* ]]; then
	CHANGES=${line##*:};
	#echo $EXEONLY
    fi
    if [[ $line == \[FaultTracer\]\ Select\ * ]]; then
	#echo "|"$CHANGES"|"$DIFF"|"
	if [[ $RANGE != "all" ]] && [[ $CHANGES == $DIFF ]]; then
	    #echo $CHANGES;
	    continue;
	fi
	IFS=' ' read -a items<<< $line
	TEST=${items[2]};
	#echo $TIME
	if [ $OLDV != . ]; then
		if [  $EXEONLY == true ]; then
		    let v++;
		fi
		eval "blockRes$EXEONLY[$v]=$TEST";
	fi
    fi
done < $1
}

# transform min to seconds
formatter () {
    selTests=${1%/*};
    allTests=${1#*/};
    TOPRINT=$(echo "scale=2;$selTests*100/$allTests"|bc);
    #TOPRINT=$selTests;
}

# print raw result matrix
printMatrix () {
    echo $SUB" "
for ((i=0;i<=v;i++))
do
    printf $i" "
for tag in true false; do
    cell=blockRes$tag[$i];
    formatter ${!cell};
    printf $TOPRINT" ";
done
    printf "\n"
done
}

#print R data
printRData (){
for ((i=0;i<=v;i++))
do
    for tag in true false;
    do
	cell=blockRes$tag[$i];
	formatter ${!cell};
	#printf ${!cell}
	printf $1"\t"$i"\t"$tag"\t"15"\t"$TOPRINT"\n"
    done
done
}

# print avg
printAvg () {
    echo $SUB" "
avgtrue=0;
avgfalse=0;
for ((i=0;i<=v;i++))
do
    for tag in true false; do
    cell=blockRes$tag[$i];
    formatter ${!cell};
    cell=avg$tag;
    val=$(echo "${!cell}+$TOPRINT"|bc);
    eval "avg$tag=$val";
    done
done
for tag in true false; do
    cell=avg$tag;
    printf $(echo "scale=2;${!cell}/$((v+1))"|bc)" ";
done
printf "\n"
}



SUBSORG=("invokebinder" "compile-testing" "commons-jxpath" "commons-fileupload" "joda-time" "commons-functor" "la4j" "commons-dbutils" "logstash-logback-encoder" "commons-validator" "asterisk-java" "commons-cli" "commons-email" "jfreechart" "commons-codec" "commons-collections" "commons-compress" "commons-lang" "commons-configuration" "commons-imaging" "commons-net" "java-apns" "commons-dbcp" "log4j" "closure-compiler" "HikariCP" "commons-math" "commons-io" "stream-lib" "OpenTripPlanner" "commons-pool" "mapdb")

SUBS=("mapdb")

printf "Sub\tVer\tTag\tTech\tTestNum\n"
for SUB in ${SUBSORG[@]}
do
#if [ $SUB ==  "mapdb" ]; then
#if [ $SUB == "stream-lib" -o  $SUB == "log4j" -o $SUB == "mapdb" ]; then
#if [ $SUB != "asterisk-java" ]; then
#    continue;
#fi
    readFile $SUB/$RTSRES $SUB
    #printMatrix
    #printAvg;
    printRData $SUB;
#fi
done
